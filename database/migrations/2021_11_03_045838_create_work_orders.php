<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
            $table->id();
            $table->string('work_type');
            $table->foreignId('organization_id');
            $table->string('project_name')->nullable();
            $table->longText('description')->nullable();
            $table->longText('location')->nullable();
            $table->float('monetary_limit', 8, 2);
            $table->date('complete_by')->nullable();
            $table->date('completed_by')->nullable();
            $table->date('service_start')->nullable();
            $table->date('service_end')->nullable();
            $table->tinyText('service_order')->nullable();
            $table->boolean('completed')->nullable();
            $table->tinyText('notify_method')->nullable();
            $table->string('notify_destination')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->timestamps();

            $table->index('organization_id');
            $table->index('work_type');
            $table->index('completed');
            $table->index('complete_by');
            $table->index('service_start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_orders');
    }
}
