<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TeoprosUserAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('account_type')->nullable();
            $table->longText('account_registration_data')->nullable();
            $table->boolean('account_registered')->default(false);
            $table->string('external_id')->nullable();
            $table->string('external_name')->nullable();
            $table->boolean('is_active')->default(true)->nullable();
            $table->index('account_registered');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('account_type');
            $table->dropColumn('account_registration_data');
            $table->dropColumn('account_registered');
            $table->dropColumn('external_id');
            $table->dropColumn('external_name');
        });
    }
}
