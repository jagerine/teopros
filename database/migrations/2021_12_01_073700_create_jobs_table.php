<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {

            $table->id();
            
            $table->foreignId('customer_id');
            $table->string('status')->nullable();
            $table->string('work_type')->nullable();
            $table->string('project_name')->nullable();
            $table->longText('description')->nullable();
            $table->longText('location')->nullable();
            $table->float('estimate', 8, 2)->nullable();
            $table->dateTime('ordered_at')->nullable();
            $table->bigInteger('ordered_by')->nullable();
            $table->float('monetary_limit', 8, 2)->nullable();
            $table->date('complete_by')->nullable();
            $table->dateTime('completed')->nullable();
            $table->bigInteger('completed_by')->nullable();
            $table->dateTime('service_start')->nullable();
            $table->dateTime('service_end')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->boolean('high_priority')->nullable();
            $table->longText('services')->nullable();
            $table->longText('materials')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();

            $table->index('customer_id');
            $table->index('status');
            $table->index('completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
