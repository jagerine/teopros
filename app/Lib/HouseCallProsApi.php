<?php 
    
namespace App\Lib; 

/**
 * Class HouseCallProsApi
 * @package App\Lib
 */
class HouseCallProsApi
{

    /**
     * @var string
     */
    private $_apiKey = 'c85186695ffa4ebe89e10a7349b830c1';

    /**
     * @var string
     */
    private $_apiUrl = 'https://api.housecallpro.com';

    /**
     * _sendApiRequest
     *
     * Sends an API request via cURL to the HouseCallPros API
     *
     * @param string $url
     * @return object
     */
    private function _sendApiRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $this->_apiKey,
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        return json_decode(curl_exec($ch));
    }

    /**
     * searchCustomers
     *
     * Searches customer by query string from the HouseCallPros API
     *
     * @param string $query
     * @return object
     */
    public function searchCustomers($query)
    {
        $url = $this->_apiUrl . '/customers?page_size=100&q=' . $query;
        return $this->_sendApiRequest($url);
    }

    /**
     * getCustomer
     * 
     * Gets a customer by ID from the HouseCallPros API
     */
    public function getCustomer($id)
    {
        $url = $this->_apiUrl . '/customers/' . $id;
        return $this->_sendApiRequest($url);
    }

    /**
     * getWorkOrders
     * 
     * Gets work orders for a customer from the HouseCallPros API
     * @param string $customerId
     * @return object
     */
    public function getWorkOrders($customerId)
    {
        $url = $this->_apiUrl . '/jobs/?customer_id=' . $customerId ;
        return $this->_sendApiRequest($url);
    }

    /**
     * getWorkOrder
     * 
     * Gets a work order by ID from the HouseCallPros API
     * @param string $workOrderId
     * @return object
     */
    public function getWorkOrder($workOrderId)
    {
        $url = $this->_apiUrl . '/jobs/' . $workOrderId ;
        return $this->_sendApiRequest($url);
    }
    
    /**
     * getWorkOrderLineItems
     * 
     * Gets work order line items for a work order from the HouseCallPros API
     * @param string $workOrderId
     * @return object
     */
    public function getWorkOrderLineItems($workOrderId)
    {
        $url = $this->_apiUrl . '/jobs/' . $workOrderId.'/line_items';
        return $this->_sendApiRequest($url);
    }
}