<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Iconlink extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($icon, $url, $text, $fill = 0, $id = '')
    {
        $this->icon = $icon;
        $this->url = $url;
        $this->text = $text;
        $this->fill = $fill;
        $this->id = $id;
    }
        
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.iconlink', [
            'icon' => $this->icon,
            'url' => $this->url,
            'text' => $this->text, 
            'fill' => $this->fill, 
            'id' => $this->id
        ]);
    }
}
