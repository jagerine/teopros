<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Forminput extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $defaults = [
            'placeholder' => '' ,
            'type' => 'text', 
            'value' => '',
        ];
        foreach($defaults as $key => $value) {
            if(!isset($this->params[$key])) {
                $this->params[$key] = $value;
            }
        }

        return view('components.forms.input', [
            'params' => $this->params, 
        ]);
    }
}
