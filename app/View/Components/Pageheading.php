<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Pageheading extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($text = null, $icon = null)
    {
        $this->text = $text;
        $this->icon = $icon;
    }
        /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.pageheading', ['text' => $this->text , 'icon' => $this->icon]);
    }
}
