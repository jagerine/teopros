<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Submitbutton extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($text = "Save")
    {
        $this->text = $text;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.submitbutton', ['text' => $this->text]);
    }
}
