<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Inputwrap extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($input, $label = null  )
    {
        $this->input = $input;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputwrap', [
            'input' => $this->input,
            'label' => $this->label,
        ]);
    }
}
