<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class WorkOrder extends Model
{
    use HasFactory;
    protected $fillable = [
        'work_type', 
        'project_name', 
        'location', 
        'description', 
        'service_start', 
        'service_end', 
        'complete_by', 
        'contact_name', 
        'contact_email',
        'contact_phone',
        'monetary_limit' 
    ];

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {     
            $model->created_by = $model->updated_by = Auth::id();
        });
        static::updating(function($model)
        {     
            $model->updated_by = Auth::id();
        });   
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public static function getTableList(){
        $headings = ['Project', 'Location', 'Start Service', 'End Service', 'Updated By', 'Updated At'];
        $data = self::orderBy('project_name')->get();
        $rows = [];
        foreach ($data as $row)     
        {
            $rows[$row['id']] = [
                '_actions' => [
                    (object) ['text' => 'Update', 'url' => '/workorders/edit/'.$row['id'], 'icon' => 'pencil-alt'], 
                    (object) ['text' => 'Delete', 'url' => '/workorders/delete/'.$row['id'], 'icon' => 'trash']
                ], 
                '_links' => [
                    'project_name' => '/workorders/show/'.$row['id']
                ],
                'project_name' => $row->project_name,
                'location' => $row->location,
                'start' => '', 
                'end' => '', 
                'updated_by' => User::find( $row->updated_by )->name,  
                'updated_at' => $row->updated_at->format(Config::get('app.machinedate'))
            ];
            if($row->service_start)
                $row['id']['start'] = $row->service_start->format(Config::get('app.machinedate'));
            if($row->service_end)
                $row['id']['end'] = $row->service_end->format(Config::get('app.machinedate'));
        }
        return (object) [
            'headings' => $headings,
            'rows' => $rows 
        ];
    }
}
