<?php

namespace App\Models;

use App\Mail\TicketNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class RequestTicket extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $fillable = [
        'user_id',
        'name',
        'email',
        'category',
        'description',
        'status',
        'priority',
        'deleted_at',
        'deleted_by',
        'created_by',
        'updated_by'
    ];

    public static function getTableList($filters = []){
        $headings = ['Customer', 'Description', 'Opened', 'Status'];

        if (empty($filters)) {
            $data = self::getUnresolved();
        }
        else {
            $query = self::leftJoin('users', 'users.id', 'request_tickets.user_id')
                ->select(
                    'request_tickets.id',
                    'request_tickets.email',
                    'request_tickets.category',
                    'request_tickets.description',
                    'request_tickets.status',
                    'request_tickets.priority',
                    'request_tickets.created_by',
                    'request_tickets.updated_by',
                    'request_tickets.created_at',
                    'request_tickets.updated_at',
                    'users.name as user_name',
                    'users.external_name as external_name'
                );
            if (!empty($filters['status'])) {
                $query->where('request_tickets.status', $filters['status']);
            }

            if(!empty($filters['customer'])){
                $users = User::where('external_id', $filters['customer'])->pluck('id');
                $query->where('request_tickets.user_id', $users);
            }

            $data = $query->orderBy('request_tickets.created_at', 'desc')->get();
        }
        $rows = [];
        foreach ($data as $row)     
        {
            $descr = $row->description;
            if(strlen($descr) > 60)
                $descr = substr($descr, 0, 60) . '...';

            $rows[$row['id']] = [
                //'_actions' => [
                    //(object) ['text' => 'Add Note', 'url' => '/tickets/add-note/'.$row['id'], 'icon' => 'chat-alt'], 
                    //(object) ['text' => 'Set Status', 'url' => '/tickets/set-status/'.$row['id'], 'icon' => 'tag']
                //], 
                '_links' => [
                    'description' => '/tickets/view/'.$row['id']
                ],
                'customer' => $row->external_name,
                'description' => $descr,
                'opened' => $row->created_at->format('m/d/Y'),
                'status' => $row->status 
            ];
            if($row->service_start)
                $row['id']['start'] = $row->service_start->format(Config::get('app.machinedate'));
            if($row->service_end)
                $row['id']['end'] = $row->service_end->format(Config::get('app.machinedate'));
        }
        return (object) [
            'headings' => $headings,
            'rows' => $rows 
        ];
    }

    public static function getUnresolved($filters = []){
        $query = self::leftJoin('users', 'users.id', 'request_tickets.user_id')
            ->select(
                'request_tickets.id',
                'request_tickets.email',
                'request_tickets.category',
                'request_tickets.description',
                'request_tickets.status',
                'request_tickets.priority',
                'request_tickets.created_by',
                'request_tickets.updated_by',
                'request_tickets.created_at',
                'request_tickets.updated_at',
                'users.name as user_name',
                'users.external_name as external_name'
            )
            ->where('status', '!=', ['Closed', 'Cancelled']);
        foreach($filters as $key => $value){
            $query->where($key, $value);
        }
        return $query->orderBy('request_tickets.created_at', 'desc')->get();
    }

    public function sendCreateNotification(){
        Mail::to($this->email)->send(new TicketNotification($this, 'create'));
    }

    public function sendUpdateNotification(HistoricalNote $historicalNote){
        Mail::to($this->email)
            ->send(new TicketNotification($this, 'update', $historicalNote));
    }

    public static function getStatuses(){
        return [
            'Open' => 'Open',
            'In Progress' => 'In Progress',
            'Closed' => 'Closed',
            'Canceled' => 'Cancelled'
        ];
    }



}
