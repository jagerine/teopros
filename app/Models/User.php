<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Mail\AccountNotification;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable implements MustVerifyEmail 
{
    use HasApiTokens, HasFactory, Notifiable;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_active', 
        'account_type',
        'external_id',
        'external_name'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getTableList($types = null){
        $headings = ['Name', 'Company', 'Email', 'Type', 'Status'];
        $data = $this->getCurrent($types);
        $rows = [];
        foreach ($data as $row)     
        {
            $rows[] = [
                '_actions' => [
                    (object) ['text' => 'Update', 'url' => '/app/edituser/'.$row['id'], 'icon' => 'pencil-alt'], 
                    (object) ['text' => 'Delete', 'url' => '/app/deleteuser/'.$row['id'], 'icon' => 'trash']
                ], 
                '_links' => [
                    'name' => 'https://pro.housecallpro.com/pro/customers/'. $row->external_id
                ],
                'name' => $row->name,
                'company' => $row->external_name,
                'email' => $row->email,
                'type' => $row->account_type,
                'status' => User::getStatus($row) 
            ];
        }
        return (object) [
            'headings' => $headings,
            'rows' => $rows 
        ];
    }

    public function getCurrent($types = ['Customer', 'Vendor']){
        return $this->whereIn('account_type', $types)
            ->orderBy('name')
            ->get();
    }

    public static function getStatus($user){
        if(empty($user->account_registered))
            return 'Pending';
        if($user->password = '000')
            return 'Inactive';
        return 'Active';    
    }

    public function isDeleted(){
        return (substr( $this->account_type, 0, 6) == 'DELETED');
    }

    public function isAdmin(){
        return ($this->account_type == 'Admin' || is_null($this->account_type));
    }

    public function sendNewAccountNotification(){
        Mail::to($this->email)->send(new AccountNotification($this));
        dd(Mail::failures());
    }

    public function userHasConfirmed(){}

}
