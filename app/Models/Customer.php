<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

use function GuzzleHttp\json_decode;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'customer_type',
        'company_name',
        'first_name',
        'last_name',
        'email',
        'phone',
        'address',
        'notes',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {     
            $model->created_by = $model->updated_by = Auth::id();
            self::setAutofilled($model);
        });
        static::updating(function($model)
        {     
            $model->updated_by = Auth::id();
            self::setAutofilled($model);
        });
        static::deleted(function($model)
        {     
            $model->deleted_by = Auth::id();
            $model->save();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public static function setAutofilled(&$model){
        $model->display_name = $model->first_name . ' ' . $model->last_name;
        if($model->customer_type == 'Business'){
            $model->display_name = $model->company_name;
        }
        $model->searchable = join(',', [
            $model->company_name,
            $model->first_name,
            $model->last_name,
            $model->email,
            preg_replace("/[^0-9]/", "", join(',', json_decode($model->phone, true))),
            $model->address,
            $model->notes
        ]);
    }

    public static function getTableList(){
        $headings = [
            'Name', 'Location', 'Updated By', 'Updated At'];
        $data = self::orderBy('display_name')->get();
        $rows = [];
        foreach ($data as $row)     
        {
            $address = json_decode($row->address, true);
            $rows[] = [
                '_actions' => [
                    (object) ['text' => 'Update', 'url' => '/organizations/edit/'.$row['id'], 'icon' => 'pencil-alt'], 
                    (object) ['text' => 'Delete', 'url' => '/organizations/delete/'.$row['id'], 'icon' => 'trash']
                ], 
                '_links' => [
                    'display_name' => '/customers/show/'.$row['id']
                ],
                'display_name' => $row->display_name,
                'location' => $address['city']. ', ' . $address['state'],
                'updated_by' => User::find( $row->updated_by )->name,  
                'updated_at' => $row->updated_at->format(Config::get('app.machinedate'))
            ];
        }
        return (object) [
            'headings' => $headings,
            'rows' => $rows 
        ];
    }

    public function getAddress(){
        return json_decode($this->address, true);
    }
    public function getAddressText(){
        $address = $this->getAddress();
        return $address['street'] . ' ' . $address['unit'] . ', ' . $address['city'] . ', ' . $address['state'] . ' ' . $address['zip'];
    }
    public function getPhones(){
        return json_decode($this->phone, true);
    }
        
}
