<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Organization extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {     
            $model->created_by = $model->updated_by = Auth::id();
        });
        static::updating(function($model)
        {     
            $model->updated_by = Auth::id();
        });   
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public static function getTableList(){
        $headings = ['Name', 'Updated By', 'Updated At'];
        $data = self::orderBy('name')->get();
        $rows = [];
        foreach ($data as $row)     
        {
            $rows[] = [
                '_actions' => [
                    (object) ['text' => 'Update', 'url' => '/organizations/edit/'.$row['id'], 'icon' => 'pencil-alt'], 
                    (object) ['text' => 'Delete', 'url' => '/organizations/delete/'.$row['id'], 'icon' => 'trash']
                ], 
                '_links' => [
                    'name' => '/organizations/show/'.$row['id']
                ],
                'name' => $row->name,
                'updated_by' => User::find( $row->updated_by )->name,  
                'updated_at' => $row->updated_at->format(Config::get('app.machinedate'))
            ];
        }
        return (object) [
            'headings' => $headings,
            'rows' => $rows 
        ];
    }

}
