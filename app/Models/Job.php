<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Job extends Model
{
    use HasFactory;
    protected $fillable = [
        'customer_id',
        'project_name',
        'description',
        'services',
        'status',
        'work_type', 
        'price',
        'location' , 
        'estimate', 
        'ordered_by',
        'ordered_at', 
        'monetary_limit', 
        'complete_by', 
        'completed', 
        'completed_by',
        'service_start', 
        'service_end' ,
        'contact_name', 
        'contact_email',
        'contact_phone',
        'high_priority',
        'materials' 
    ];

    public function getCustomer()
    {
        return Customer::find($this->customer_id);
    }   

    public function getLocation(){
        return json_decode($this->location, true);
    }
    public function getLocationText(){
        $address = $this->getLocation();
        return $address['street'] . ' ' . $address['unit'] . ', ' . $address['city'] . ', ' . $address['state'] . ' ' . $address['zip'].'<br>'.$address['description'];
    }

    public function getFiles(){
        return [];
    }

}
