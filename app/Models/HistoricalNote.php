<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class HistoricalNote extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $fillable = [
        'object_type',
        'object_id',
        'note',
        'deleted_at',
        'deleted_by',
        'created_by',
        'updated_by'
    ];

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {     
            $model->created_by = $model->updated_by = Auth::id();
        });
        static::updating(function($model)
        {     
            $model->updated_by = Auth::id();
        });
        static::deleted(function($model)
        {     
            $model->deleted_by = Auth::id();
            $model->save();
        });
    }

    public function user()
    {
        return User::find($this->created_by);
    }
    
}
