<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TicketNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($requestTicket, $type, $historicalNote = null)
    {
        $this->requestTicket = $requestTicket;
        $this->type = $type;
        $this->historicalNote = $historicalNote;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = url('/tickets/view/'.$this->requestTicket->id);
        return $this->subject('Request Ticket #'.$this->requestTicket->id.' has been '.$this->type.'d.')->markdown('mail.ticketnotification', [
            'requestTicket' => $this->requestTicket,
            'type' => $this->type,
            'historicalNote' => $this->historicalNote, 
            'displayableActionUrl' => $url, 
            'actionUrl' => $url,
        ]);
    }
}
