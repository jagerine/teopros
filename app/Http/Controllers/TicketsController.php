<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HouseCallProsApi;
use App\Models\RequestTicket;
use App\Models\HistoricalNote;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class TicketsController extends Controller
{
    
    public function index(){
        if (!Auth::user()->isAdmin()) {
            return redirect('/');
        }
        $query = request()->all();
        $filters = [];
        $filterOptions = ['status', 'customer'];
        foreach($filterOptions as $filterOption){
            if(!empty($query[$filterOption])){
                $filters[$filterOption] = $query[$filterOption];
            }
        }
        $statusOptions = RequestTicket::getStatuses();
        $requestTickets = RequestTicket::getTableList($filters);
        $users = User::join('request_tickets', 'request_tickets.user_id', 'users.id')
            ->distinct()
            ->orderBy('users.external_name', 'asc')
            ->pluck('users.external_name', 'users.external_id')
            ->toArray();
        return view('tickets.index', compact('requestTickets', 'statusOptions', 'users'));
    }

    public function view($requestTicketId){
        $authUser = Auth::user();
        $requestTicket = RequestTicket::find($requestTicketId);
        $ticketStatuses = RequestTicket::getStatuses();
        $historicalNotes = HistoricalNote::where('object_type', 'RequestTicket')
            ->where('object_id', $requestTicketId)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('tickets.view', compact(
            'requestTicket', 
            'historicalNotes', 
            'authUser', 
            'ticketStatuses'
        ));
    }

    public function addNote($requestTicketId)
    {
        $post = request()->all();
        if(!empty($post)){
            $historicalNote = new HistoricalNote();
            $historicalNote->object_type = 'RequestTicket';
            $historicalNote->object_id = $requestTicketId;
            $historicalNote->note = $post['note'];
            $historicalNote->save();

            RequestTicket::find($requestTicketId)->sendUpdateNotification($historicalNote);
            return redirect('/tickets/view/'.$requestTicketId)->with('message', 'Note has been added successfully.');
        }
        return view('tickets.addNote');
    }

    public function deleteHistoricalNote($historicalNoteId)
    {
        $historicalNote = HistoricalNote::find($historicalNoteId);
        $ticketId = $historicalNote->object_id;
        $historicalNote->delete();
        if(request()->ajax()){
            return response()->json(['success' => 'Historical note has been deleted successfully.']);
        }
        return redirect("/tickets/view/$ticketId")->with('message', 'Note has been deleted successfully.');
    }

    public function setStatus($requestTicketId){
        $post = request()->all();
        if(!empty($post)){
            $requestTicket = RequestTicket::find($requestTicketId);
            $requestTicket->status = $post['status'];
            $requestTicket->save();
            return redirect('/tickets/view/'.$requestTicketId)->with('message', 'Status has been updated successfully.');
        }
        redirect('/tickets/view/'.$requestTicketId);
    }

    public function getWorkOrderName($workOrderId){
        $workOrder = (new HouseCallProsApi())->getWorkOrder($workOrderId);
        return view('tickets.getWorkOrderName', ['name' => $workOrder->description]);
    }
}
