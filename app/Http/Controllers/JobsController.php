<?php

namespace App\Http\Controllers;
use App\Models\Job;
use App\Models\Customer;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    public function __invoke($page){
        return view('pages.' . $page);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //$jobs = Job::getTableList();
        $jobs = [];
        return view('jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customerID)
    {
        $customer = Customer::find($customerID);
        return view('jobs.createOrUpdate', ['heading' => 'New Estimate / Job', 'customer' => $customer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $save = $request->all();
        array_shift($save['line_items']);
        $save['services'] = json_encode($save['line_items']);
        unset($save['line_items']);
        $save['location'] = json_encode($save['location']);
        $save['customer_id'] = 1;
        
        $dates = ['ordered_at', 'complete_by', 'service_start', 'service_end'];
        foreach($dates as $date){
            if(isset($save[$date])){
                $save[$date] = date('Y-m-d', strtotime($save[$date]));
            }
        }

        
        //$validated = $this->validate($request, [
        //    "name" => "required|min:3|string", 
        //]);
        //dd($save); 
        Job::create($save);
        return redirect('/jobs')->with('message', 'Job created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function show($jobID)
    {
        $job = Job::find($jobID);
        return view('jobs.show', compact('job'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function edit($jobID) {
        $heading = 'Update Job';
        $job = Job::find($jobID);
        return view('jobs.createOrUpdate', compact('job', 'heading'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job, $jobID)
    {
        $validated = $this->validate($request, [
            "name" => "required|min:3|string"
        ]);
        $job->find($jobID)->update($validated);
        return back()->with('message', 'Job updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function destroy($jobID)
    {
        $job = Job::find($jobID);
        $job->delete();
        return back()->with('message', 'Job deleted successfully');
    }
}
