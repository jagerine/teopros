<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function __invoke($page){
        return view('pages.' . $page);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizations = Organization::getTableList();
        return view('organizations.index', compact('organizations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organizations.createOrUpdate', ['heading' => 'Create Organization']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "name" => "required|min:3|string", 
        ]);
        Organization::create($validated);
        return redirect('/organizations')->with('message', 'Organization created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function show($organizationID)
    {
        $organization = Organization::find($organizationID);
        return view('organizations.show', compact('organization'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function edit($organizationID) {
        $heading = 'Update Organization';
        $organization = Organization::find($organizationID);
        return view('organizations.createOrUpdate', compact('organization', 'heading'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization, $organizationID)
    {
        $validated = $this->validate($request, [
            "name" => "required|min:3|string"
        ]);
        $organization->find($organizationID)->update($validated);
        return back()->with('message', 'Organization updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy($organizationID)
    {
        $organization = Organization::find($organizationID);
        $organization->delete();
        return back()->with('message', 'Organization deleted successfully');
    }
}
