<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Job;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function __invoke($page){
        return view('pages.' . $page);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::getTableList();
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.createOrUpdate', ['heading' => 'Add Customer']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "company_name" => "required|min:3|string", 
            "first_name" => "required|min:2|string", 
            "last_name" => "required|min:2|string", 
        ]);

        $save = $request->all();
        $searchableString = join(' ',[
            $save['company_name'],
            $save['first_name'],
            $save['last_name'],
            preg_replace("/[^0-9]/", "", join(',', $save['phone'])),
            $save['email'],
            join(',', $save['address']) 
        ]);
        $save['address'] = json_encode($save['address']);
        $save['phone'] = json_encode($save['phone']);

        unset($save['_token']);
        Customer::create($save);

        return redirect('/customers')->with('message', 'Customer created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function show($customerID)
    {
        $customer = Customer::find($customerID);
        $jobs = Job::where('customer_id', $customerID)->get();
        return view('customers.show', compact('customer', 'jobs'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($customerID) {
        $heading = 'Update Customer';
        $customer = Customer::find($customerID);
        $customer->address = json_decode($customer->address);
        $customer->phone = json_decode($customer->phone);
        return view('customers.createOrUpdate', compact('customer', 'heading'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer, $customerID)
    {
        $validated = $this->validate($request, [
            "name" => "required|min:3|string"
        ]);
        $customer->find($customerID)->update($validated);
        return back()->with('message', 'Customer updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($customerID)
    {
        $customer = Customer::find($customerID);
        $customer->delete();
        return back()->with('message', 'Customer deleted successfully');
    }
}
