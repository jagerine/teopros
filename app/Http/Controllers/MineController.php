<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use \App\Lib\HouseCallProsApi;
use \App\Models\RequestTicket;
use Illuminate\Support\Facades\Auth;
use PDF;

class MineController extends Controller
{
    public function index(){

        $requestTickets = RequestTicket::where('user_id', Auth::id())
            ->where('status', '!=', 'Closed')
            ->orderBy('updated_at', 'desc')
            ->get();
        return view('mine.index', compact('requestTickets'));
    }

    public function addRequest(){

        $post = request()->all();
        if(!empty($post)){
            $requestTicket = new RequestTicket();
            $requestTicket->user_id = Auth::user()->id;
            $requestTicket->name = $post['name'];
            $requestTicket->email = $post['email'];
            $requestTicket->category = $post['category'];
            if($requestTicket->category == 'Work Orders'){
                $requestTicket->external_id = $post['external_id'];
            }
            $requestTicket->description = $post['description'];
            $requestTicket->status = 'Open';
            $requestTicket->priority = 'Normal';
            $requestTicket->save();
            $requestTicket->sendCreateNotification();
            return redirect('/')->with('message', 'Request #'.$requestTicket->id.' has been submitted successfully.');
        }

        return view('mine.addRequest');
    }

    public function workOrderTicketSelection(Request $request){
        $return = [];
        $HouseCallPros = new HouseCallProsApi();
        $houseCallProsId = $request->user()->external_id;
        $hcpWorkOrders = $HouseCallPros->getWorkOrders($houseCallProsId);
        foreach($hcpWorkOrders->jobs as $hcpWorkOrder){
            $return[] = [
                'id' => $hcpWorkOrder->id,
                'name' => $hcpWorkOrder->description
            ];
        }
        return response()->json($return);
    }

    public function workOrders(Request $request){
        $HouseCallPros = new HouseCallProsApi();
        $houseCallProsId = $request->user()->external_id;
        $hcpWorkOrders = $HouseCallPros->getWorkOrders($houseCallProsId);
        return view('mine.workOrders', ['hcpWorkOrders' => $hcpWorkOrders]);
    }

    public function viewWorkOrder($workOrderId){
        return view('mine.viewWorkOrder', ['workOrderId' => $workOrderId]);
    }
    
    public function viewWorkOrderDetails($workOrderId){
        $workOrder = (new HouseCallProsApi())->getWorkOrder($workOrderId);
        $storedResponseKey = $this->_storeApiResponse($workOrder);
        $html = view('mine.viewWorkOrderDetails', ['workOrder' => $workOrder]);
        return response()->json([
            'html' => $html->render(), 
            'storedResponseKey' => $storedResponseKey 
        ]);
    }

    public function viewWorkOrderLineItems($workOrderId){
        $workOrderLineItems = (new HouseCallProsApi())->getWorkOrderLineItems($workOrderId);
        $storedResponseKey = $this->_storeApiResponse($workOrderLineItems);
        $html = view('mine.viewWorkOrderLineItems', ['workOrderLineItems' => $workOrderLineItems]);
        return response()->json([
            'html' => $html->render(),
            'storedResponseKey' => $storedResponseKey
        ]);
    }

    public function viewWorkOrderPdf($workOrderId){
        
        $httpQuery = request()->all();
        $detailsKey = $httpQuery['DetailsKey'];
        $lineItemsKey = $httpQuery['LineItemsKey'];
        $workOrderDetails = $this->_getStoredApiResponse($detailsKey);
        $workOrderLineItems = $this->_getStoredApiResponse($lineItemsKey);

        $pdf = PDF::loadView('mine.viewWorkOrderPdf', [
            'workOrderDetails' => $workOrderDetails, 
            'workOrderLineItems' => $workOrderLineItems
        ]);

        return $pdf->download('WorkOrder-'.$workOrderId.'.pdf');
    }

    private function _storeApiResponse($response){
        $randomKey = bin2hex(random_bytes(20));
        session()->put($randomKey, $response);
        return $randomKey;
    }
    private function _getStoredApiResponse($storedResponseKey){
        return session()->get($storedResponseKey);
    }

}
