<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Workorder;
use Carbon\Carbon;

class WorkordersController extends Controller
{
    public function __invoke($page){
        return view('pages.' . $page);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workorders = WorkOrder::getTableList();
        return view('workorders.index', compact('workorders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('workorders.createOrUpdate', ['heading' => 'Create Work Order']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [   
            'work_type' => "required|min:3|string", 
            "project_name" => "required|min:3|string", 
            "description" => "required|min:3|string", 
            "location" => "required|min:3|string", 
            "monetary_limit" => "required|min:3|string", 
            "complete_by" => "nullable|date", 
            "service_start" => "nullable|date", 
            "service_end" => "nullable|date", 
            "contact_name" => "required|min:3|string", 
            "contact_email" => "required|min:3|string",
            "contact_phone" => "required|min:3|string" 
        ]);
        
        $dateValues = ['complete_by', 'service_start', 'service_end'];
        foreach($dateValues as $dateValue){
            if(!empty($validated[$dateValue])){
                $validated[$dateValue] = Carbon::parse($validated[$dateValue])->format('Y-m-d');
            }
        }
        WorkOrder::create($validated);
        return redirect('/workorders')->with('message', 'Work order created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Workorder $workorder
     * @return \Illuminate\Http\Response
     */
    public function show($workorderID)
    {
        $workorder = WorkOrder::find($workorderID);
        return view('workorders.show', compact('workorder'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Workorder $workorder
     * @return \Illuminate\Http\Response
     */
    public function edit($workorderID){
        $heading = 'Update Work Order';
        $workorder = WorkOrder::find($workorderID);
        return view('workorders.createOrUpdate', compact('organization', 'heading'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Workorder $workorder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $workorder, $workorderID)
    {
        $validated = $this->validate($request, [
            "name" => "required|min:3|string"
        ]);
        $workorder->find($workorderID)->update($validated);
        return back()->with('message', 'Organization updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $workorderID
     * @return \Illuminate\Http\Response
     */
    public function destroy($workorderID)
    {
        $workorder = WorkOrder::find($workorderID);
        $workorder->delete();
        return back()->with('message', 'Organization deleted successfully');
    }
}
