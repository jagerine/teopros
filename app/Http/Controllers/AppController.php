<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Exceptions\Handler as Exception;
use Illuminate\Http\Request;
use App\Lib\HouseCallProsApi;
use App\Models\HistoricalNote;
use Illuminate\Support\Facades\Mail;
use App\Models\RequestTicket;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

class AppController extends Controller
{

    public function test(){
        $ticket = RequestTicket::find(1100202);
        $ticket->sendCreateNotification();
        dd('stop');
    }

    public function completeRegistration($emailToken)
    {
        $email = base64_decode($emailToken);
        $key = request()->get('key');
        $user = User::where('email', $email)->first();
        if(empty($user)){
            return view('errors.custom', ['message' => 'Your token is invalid.', 'errorCode' => 498 ]);
        }
        if(!empty($user->account_registered)){
            return view('errors.custom', ['message' => 'This token has already been used.', 'errorCode' => 410 ]);
        }
        if(empty($user->account_registration_data) || $user->account_registration_data != $key){
            return view('errors.custom', ['message' => 'Your token is invalid.', 'errorCode' => 498 ]);
        }

        if(request()->isMethod('post')){
            $post = request()->all();
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->is_active = $user->account_registered = 1;
            $user->account_registration_data = null;
            $user->password = bcrypt($post['password']);
            $user->save();
            return redirect('/login')->with('success', 'Your account has been activated. Please login.');
        }
        
        return view('app.completeRegistration', [
            'email' => $email,
            'key' => $key, 
            'emailToken' => $emailToken
        ]);
        
    }

    public function search()
    {
        $searchTerm = request()->get('app-search');
        $requestTickets = RequestTicket::leftJoin('users', 'request_tickets.user_id', 'users.id')
            ->select('request_tickets.*', 'users.external_name')
            ->where('request_tickets.description', 'like', '%'.$searchTerm.'%')
            ->orWhere('request_tickets.email', 'like', '%'.$searchTerm.'%')
            ->orWhere('users.email', 'like', '%'.$searchTerm.'%')
            ->orWhere('users.external_name', 'like', '%'.$searchTerm.'%')
            ->orderBy('request_tickets.created_at', 'desc')
            ->get();
        $users = User::where('users.name', 'like', '%'.$searchTerm.'%')
            ->orWhere('users.external_name', 'like', '%'.$searchTerm.'%')
            ->orWhere('users.email', 'like', '%'.$searchTerm.'%')
            ->orderBy('users.name', 'asc')
            ->get();
        return view('app.search', compact('requestTickets', 'users', 'searchTerm'));
    }

    public function addNote(){
        $post = request()->all();
        if(!empty($post)){
            $historicalNote = new HistoricalNote();
            $historicalNote->object_type = $post['object_type'];
            $historicalNote->object_id = $post['object_id'];
            $historicalNote->note = $post['note'];
            $historicalNote->save();
            $redirect = $post['redirect'] ?? 'back';
            if(strtolower($redirect) == 'back'){
                return redirect()->back()->with('message', 'Note has been added successfully.');
            } else {
                return redirect($redirect)->with('message', 'Note has been added successfully.');
            }
        }
        return redirect('/');
    }
    public function deleteHistoricalNote($historicalNoteId)
    {
        $historicalNote = HistoricalNote::find($historicalNoteId);
        $historicalNote->delete();
        if(request()->ajax()){
            return response()->json(['success' => 'Historical note has been deleted successfully.']);
        }
        return redirect()->back()->with('message', 'Note has been deleted successfully.');
    }

    public function user($userId)
    {
        $user = User::find($userId);
        $historicalNotes = HistoricalNote::where('object_type', 'User')
            ->where('object_id', $userId)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('app.user', compact('user', 'historicalNotes'));
    }

    public function admin(){
        $users = (new \App\Models\User())->getCurrent();
        $requestTickets = RequestTicket::getUnresolved();
        return view('app.admin', ['users' => $users , 'requestTickets' => $requestTickets]); 
    }

    public function toggleUserActivation($userId){
        $user = User::find($userId);
        $user->is_active = !$user->is_active;
        $newStatus = ($user->is_active) ? 'active' : 'inactive';
        $user->save();

        $historicalNote = new HistoricalNote();
        $historicalNote->object_type = 'User';
        $historicalNote->object_id = $userId;
        $historicalNote->note = 'Account was set to '.$newStatus.'.';
        $historicalNote->save();

        return redirect()->back()->with('message', 'User status has been set to '.$newStatus.'.');
    }

    public function updateAccount(Request $request, $userId){
        $validated = $this->validate($request, [
            "account_type" => "required|string", 
            "name" => "required|min:3|string", 
            "email" => "required|email", 
            'external_id' => 'string',
            'external_name' => 'string',
        ]);
        $user = User::find($userId);
        $post = $request->all();

        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->account_type = $validated['account_type'];

        if (!empty($post['change_account'])) {
            $user->external_id = $validated['external_id'];
            $user->external_name = $validated['external_name'];
        }       
        $user->save();
        
        $historicalNote = new HistoricalNote();
        $historicalNote->object_type = 'User';
        $historicalNote->object_id = $userId;
        $historicalNote->note = 'Account was updated.';
        $historicalNote->save();

        return redirect('/app/user/'.$user->id)->with('message', 'Account has been updated successfully.');
    }
    
    public function addAccount(){
        return view('app.addOrEditAccount', [
            'randomPassword' => $this->_getRandomPassword(), 
            'isEdit' => false
        ]);
    }
    private function _getRandomPassword() {
        $passwordLength = 10;
        $characterPool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $store = [];
        $length = strlen($characterPool) - 1;
        while($passwordLength--) {
            $store[] = substr($characterPool, mt_rand(0, $length), 1);
        }
        return join('', $store);
    }

    public function editUser($userId){
        $user = (new \App\Models\User())->find($userId);
        return view('app.addOrEditAccount', [
            'user' => $user, 
            'randomPassword' => '', 
            'isEdit' => true
        ]);
    }

    public function searchHcp(Request $request){
        $query = trim($request->all()['q']);
        $HouseCallPros = new HouseCallProsApi();
        $customers = $HouseCallPros->searchCustomers($query);
        return view('app.searchhcp', ['customers' => $customers, 'query' => $query]);
    }

    public function storeAccount(Request $request){
        $validated = $this->validate($request, [
            "account_type" => "required|string", 
            "name" => "required|min:3|string", 
            "email" => "required|email|unique:users,email", 
            "pass" => "required|string", 
            'external_id' => 'required|string',
            'external_name' => 'string',
        ]);

        $user = new \App\Models\User();
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->password = bcrypt($validated['pass']);
        $user->external_id = $validated['external_id'];
        $user->external_name = $validated['external_name'];
        $user->account_type = $validated['account_type'];
        $user->account_registration_data = bin2hex(random_bytes(20));
        $user->save();
        //$user->sendNewAccountNotification();
        event(new Registered($user));
        return redirect()->back()->with('message', 'User has been created successfully.');
    }

    public function mailNewAccountNotification(){
        $user = \App\Models\User::where('account_type', '=', 'Customer')->first();
        Mail::to($user)
            ->send(new \App\Mail\NewAccountNotification($user));
        die('done');
    }

    public function showCustomer(Request $request){
        $user = \App\Models\User::where('account_type', '=', 'Customer')->first();
        $HouseCallPros = new HouseCallProsApi();
        $customer = $HouseCallPros->getCustomer($user->external_id);
        dd($customer); 
        return view('app.searchhcp', []);
    }
    
}
