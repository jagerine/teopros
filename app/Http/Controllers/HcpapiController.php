<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HcpapiController extends Controller
{
    
    private $apiKey = 'c85186695ffa4ebe89e10a7349b830c1';
    
    public function customers()
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, 'https://api.housecallpro.com/customers');
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $this->apiKey,
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //url_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($ch);
        dd(json_decode($result)); 
    }

    public function jobs(){
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, 'https://api.housecallpro.com/jobs');
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $this->apiKey,
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //url_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($ch);
        dd(json_decode($result)); 
    }


    /* curl --request GET \
  --url https://api.housecallpro.com/customers \
  --header 'Authorization: ' \
  --header 'Content-Type: application/json' */
}
