<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RequireAccountRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if (!$user->account_registered) {
            return redirect('/complete-registration/' . base64_encode($user->email).'?key='.$user->account_registration_data);
        }

        return $next($request);
    }
}
