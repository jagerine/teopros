<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');*/

Route::get('/dashboard', function(){
    return redirect('/');  
})->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/', function(){
    $user = Auth::user();
    if($user->isDeleted()){
        return redirect('/app/deregistered');
    }
    if ($user->account_type == 'Admin') {
        return redirect('/app/admin');
    }
    if (in_array($user->account_type, ['Customer', 'Vendor'])) {
        return redirect('/mine');
    }   
})->middleware(['auth', 'verified', 'require-registration'])->name('dashboard');

Route::get('/app/test', 'App\Http\Controllers\AppController@test')->middleware(['auth', 'verified'])->name('app.test');
Route::match(['GET', 'POST'], '/complete-registration/{token}', 'App\Http\Controllers\AppController@completeRegistration')->name('app.complete-registration');

Route::get('/app/admin', 'App\Http\Controllers\AppController@admin')->middleware(['auth', 'verified'])->name('app.admin');
Route::get('/app/showcustomer', 'App\Http\Controllers\AppController@showCustomer')->middleware(['auth', 'verified'])->name('app.showCustomer');
Route::get('/app/addaccount', 'App\Http\Controllers\AppController@addAccount')->middleware(['auth', 'verified'])->name('app.addaccount');
Route::get('/app/user/{userId}', 'App\Http\Controllers\AppController@user')->middleware(['auth', 'verified'])->name('app.user');
Route::get('/app/edituser/{userId}', 'App\Http\Controllers\AppController@editUser')->middleware(['auth', 'verified'])->name('app.edituser');
Route::get('/app/search-hcp', 'App\Http\Controllers\AppController@searchHcp')->middleware(['auth', 'verified'])->name('app.search-hcp');
Route::post('/app/store-account', 'App\Http\Controllers\AppController@storeAccount')->middleware(['auth', 'verified'])->name('app.store-account');
Route::get('/app/sendmail', 'App\Http\Controllers\AppController@mailNewAccountNotification')->middleware(['auth', 'verified'])->name('app.sendmail');
Route::post('/app/add-note', 'App\Http\Controllers\AppController@addNote')->middleware(['auth', 'verified'])->name('app.add-note');
Route::get('/app/delete-historical-note/{id}', 'App\Http\Controllers\AppController@deleteHistoricalNote')->middleware(['auth', 'verified'])->name('app.deleteHistoricalNote');
Route::post('/app/update-account/{id}', 'App\Http\Controllers\AppController@updateAccount')->middleware(['auth', 'verified'])->name('app.update-account');
Route::get('/app/toggle-user-activation/{id}', 'App\Http\Controllers\AppController@toggleUserActivation')->middleware(['auth', 'verified'])->name('app.toggleUserActivation');

Route::get('/search', 'App\Http\Controllers\AppController@search')->middleware(['auth', 'verified'])->name('app.search');

Route::get('/mine', 'App\Http\Controllers\MineController@index')->middleware(['auth', 'verified'])->name('mine.index');
Route::get('/mine/work-orders', 'App\Http\Controllers\MineController@workOrders')->middleware(['auth', 'verified'])->name('mine.workOrders');
Route::get('/mine/view-work-order/{workOrderId}', 'App\Http\Controllers\MineController@viewWorkOrder')->middleware(['auth', 'verified'])->name('mine.viewWorkOrder');
Route::get('/mine/view-work-order-details/{workOrderId}', 'App\Http\Controllers\MineController@viewWorkOrderDetails')->middleware(['auth', 'verified'])->name('mine.viewWorkOrderDetails');
Route::get('/mine/view-work-order-line-items/{workOrderId}', 'App\Http\Controllers\MineController@viewWorkOrderLineItems')->middleware(['auth', 'verified'])->name('mine.viewWorkOrderLineItems');
Route::get('/mine/view-work-order-pdf/{workOrderId}', 'App\Http\Controllers\MineController@viewWorkOrderPdf')->middleware(['auth', 'verified'])->name('mine.viewWorkOrderPdf');
Route::match(['GET','POST'], '/mine/add-request', 'App\Http\Controllers\MineController@addRequest')->middleware(['auth', 'verified'])->name('mine.addRequest');
Route::get('/mine/work-order-ticket-selection', 'App\Http\Controllers\MineController@workOrderTicketSelection')->middleware(['auth', 'verified'])->name('mine.workOrderTicketSelection');

Route::get('/tickets', 'App\Http\Controllers\TicketsController@index')->middleware(['auth', 'verified'])->name('tickets');
Route::get('/tickets/view/{id}', 'App\Http\Controllers\TicketsController@view')->middleware(['auth', 'verified'])->name('tickets.view');
Route::post('/tickets/add-note/{id}', 'App\Http\Controllers\TicketsController@addNote')->middleware(['auth', 'verified'])->name('tickets.addNote');
Route::get('/tickets/delete-historical-note/{id}', 'App\Http\Controllers\TicketsController@deleteHistoricalNote')->middleware(['auth', 'verified'])->name('tickets.deleteHistoricalNote');
Route::post('/tickets/set-status/{id}', 'App\Http\Controllers\TicketsController@setStatus')->middleware(['auth', 'verified'])->name('tickets.setStatus');
Route::get('/tickets/get-work-order-name/{id}', 'App\Http\Controllers\TicketsController@getWorkOrderName')->middleware(['auth', 'verified'])->name('tickets.getWorkOrderName');


require __DIR__.'/auth.php';
