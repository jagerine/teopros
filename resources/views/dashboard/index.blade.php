@extends('layouts.app', ['header' => 'Dashboard'])

@section('content')

<a href="/" class="text-lg px-3 py-2 border border-gray-300 bg-blue-50 rounded-md text-blue-900 hover:text-gray-50 hover:bg-gray-900 mr-2">New Work Order</a>
<hr class="my-3">
<div class="container-fluid mt--7">
    <div class="grid gap-4 grid-cols-1 md:grid-cols-2">
        <div class="dashcard border border-gray-100 bg-gray-50 rounded-md shadow-md p-2">
            <x-pageheading text="Invoices" icon="currency-dollar" />
        </div>
        <div class="dashcard border border-gray-100 bg-gray-50 rounded-md shadow-md p-2">
            <x-pageheading text="Work Orders" icon="clipboard-list" />
            <div class="p-2">
                <ul>
                    <li>
                        <a href="/workorders/index/open" class="page-link">
                            <span>&gt; Open Work Orders ()</span> 
                        </a>
                    </li>
                    <li>
                        <a href="/workorders/index/closed"  class="page-link">
                            <span>&gt; All Work Orders ()</span> 
                        </a>
                    </li>
                </ul>
                <hr>
                <h3 class="pt-3">Create Work Order</h3>
                {{-- lists('title', 'name') --}}
                {{ Form::open(['url' => '/workorders/store', 'method' => 'get']) }}

                    
                <select name="organization_id"
                    type="select"
                    placeholder="Organization"
                    class="
                    mb-1
                    px-2
                    py-1
                    placeholder-gray-400
                    text-gray-600
                    relative
                    bg-white bg-white
                    rounded
                    text-sm
                    border border-gray-400
                    outline-none
                    focus:outline-none focus:ring
                    w-full
                    pr-10
                    "
                />
                <span
                    class="
                    z-10
                    h-full
                    leading-snug
                    font-normal
                    absolute
                    text-center text-gray-400
                    absolute
                    bg-transparent
                    rounded
                    text-base
                    items-center
                    justify-center
                    w-8
                    right-0
                    pr-2
                    py-1
                    "
                >
                
                <x-submitbutton text="Go" />

                {{ Form::close() }}
            </div>
            
        </div>
        
        
    </div>
    

@endsection