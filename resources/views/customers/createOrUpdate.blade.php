@extends('layouts.app', ['header' => $heading])

@section('content')
    
    @if(isset($customer))
        {{ Form::model($customer, ['url' => ['customers/update/'. $customer->id]]) }}
    @else
        {{ Form::open(['url' => 'customers/store']) }}
    @endif

    <span class="w-full lg:w-2/6 inline-block">
        
        <x-forminput 
            :params="[
                'type' => 'select', 
                'name' => 'customer_type', 
                'label' => 'Type', 
                'options' => ['' => '', 'Personal' => 'Personal', 'Business' => 'Business']
            ]" 
        />
    </span>
    <hr class="m-2">
    <span class="w-full lg:w-2/6 inline-block">
        <x-forminput :params="['name' => 'company_name', 'label' => 'Company Name']" /> 
    </span>
    <div class="flex flex-col md:flex-row">
        <div class="">
            <x-forminput :params="['name' => 'first_name', 'label' => 'First Name']" /> 
        </div>
        <div class="">
            <x-forminput :params="['name' => 'last_name', 'label' => 'Last Name']" /> 
        </div>
    </div>
    <hr class="visible md:invisible" />
    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-72">
            <x-forminput :params="['name' => 'address[street]', 'label' => 'Address']" /> 
        </div>
        <div class="w-full md:w-32">
            <x-forminput :params="['name' => 'address[unit]', 'label' => 'Unit']" /> 
        </div>
    </div>
    
    <div class="flex flex-col md:flex-row">
        <div class="">
            <x-forminput :params="['name' => 'address[city]', 'label' => 'City']" /> 
        </div>
        <div class="">
            <x-forminput :params="['name' => 'address[state]', 'label' => 'ST']" /> 
        </div>
        <div class="">
            <x-forminput :params="['name' => 'address[zip]', 'label' => 'Zip']" /> 
        </div>
    </div>
    <hr class="visible md:invisible" />
    <div class="flex flex-col md:flex-row">
        <div class="">
            <x-forminput :params="['name' => 'phone[main]', 'label' => 'Main Phone']" /> 
        </div>
        <div class="">
            <x-forminput :params="['name' => 'phone[alternate]', 'label' => 'Alternate Phone']" /> 
        </div>
        <div class="">
            <x-forminput :params="['name' => 'phone[fax]', 'label' => 'Fax']" /> 
        </div>
    </div>

    <div class="w-full lg:w-2/6 block">
        <x-forminput :params="['name' => 'email', 'label' => 'Email']" /> 
    </div>
    
    <hr>

    <div class="w-full lg:w-3/6 block">
        <x-forminput :params="['type' => 'textarea', 'name' => 'notes', 'label' => 'Notes']" /> 
    </div>
        <x-submitbutton />

        {{ Form::close() }}

@endsection