@extends('layouts.app', ['header' => 'Customer: '.$customer->display_name])

@section('content')
    <x-utilitymenu :items="[
        (object) ['text' => 'New Estimate / Job', 'url' => '/jobs/create/'.$customer->id, 'icon' => 'plus-circle'] , 
        (object) ['text' => 'Update Customer', 'url' => '/customers/edit/'.$customer->id, 'icon' => 'pencil-alt'] , 
        (object) ['text' => 'Delete', 'url' => '/customers/destroy/'.$customer->id, 'icon' => 'trash'] 
    ]" />

<div class="border-t border-gray-200">
    @php
        $fields = [
            'Type' => $customer->customer_type,
            'Name' => $customer->display_name, 
            'Email' => $customer->email, 
            'Phone(s)' => '', 
            'Location' => '<span class="address-container mr-2">'.$customer->getAddressText().'</span>', 
        ];
        foreach($customer->getPhones() as $phoneType => $phone) {
            if(trim($phone) == '')
                continue;
            $fields['Phone(s)'] .= ucwords($phoneType).': <a class="page-link" href="tel:'.$phone.'">'.$phone . '</a><br>';
        }
    @endphp
    <dl>
        @foreach ($fields as $key => $value)
            
        <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b bg-gray-50">
            <dt class="text-sm font-medium text-gray-500">
                {{ $key }}
            </dt>
            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {!! $value !!}
            </dd>
        </div>

        @endforeach
    </dl>
</div>
    <hr class="my-4">
    <div class="flex flex-col md:flex-row gap-3 m-1">
        <div class="flex-grow bg-gray-50 rounded-md p-2">
            <h3 class="text-lg text-yellow-700">Work Orders</h3>
            @if(empty($jobs))
                <em class="text-gray-500">None</em>
            @else

                @foreach ($jobs as $job)    

                    <div class="flex flex-col md:flex-row line-item-row text-sm">
                        <div class="w-full md:w-6/12">
                            <div class="text-gray-500 text-xs">
                                {{ $job->status }}
                            </div>
                            <a href="/jobs/show/{{ $job->id }}" class="font-medium underline hover:font-bold">{{ $job->project_name }}</a>
                            <div class="text-gray-500 text-xs">
                                {{ $job->getCustomer()->display_name }}
                            </div>
                        </div>
                        <div class="w-full md:w-6/12">
                            <div class="text-gray-500 text-xs invisible md:visible">&nbsp; </div> 
                            Complete by: {{ date('m/d/Y', strtotime($job->complete_by)) }}
                        </div>
                        
                    </div>  
                    <hr>

                @endforeach

            @endif
        </div>
        <div class="flex-grow bg-gray-50 rounded-md p-2">
            <h3 class="text-lg text-yellow-700">Invoices</h3>
            @if(empty($invoices))
                <em class="text-gray-500">None</em>
            @else

            @endif
        </div>
    </div>


    <div class="invisible hidden" id="MaplinkTemplate">
        <x-maplink search="___" />
    </div>
    <script>
        $('.address-container').each(function(){
            var address = $(this).text();
            var link = $('#MaplinkTemplate').html().replace('___', address);
            $(link).insertAfter(this);
        });
        

    </script>

@endsection