@extends('layouts.app', ['header' => 'Customers'])

@section('content')

     
    <x-utilitymenu :items="[
        (object) ['text' => 'Add Customer', 'url' => route('customers.create'), 'icon' => 'plus-circle'] 
    ]" />
    <div class="flex flex-col md:flex-row">
        <!-- <div class="md:w-3/12">
           asdf
        </div> -->
        <div class="md:w-9/12 text-xs md:text-sm">
            <x-datatable :data="$customers" textFilter="1" />
        </div>
    </div>
     
    


@endsection