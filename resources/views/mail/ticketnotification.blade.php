@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
# @lang('Hello from TeoPros!')
@endif

{{-- Intro Lines --}}
Request ticket #{{ $requestTicket->id }} has been {{ $type }}d.

@if(!empty($historicalNote))
<em>{{ nl2br($historicalNote->note) }}</em>

@endif

@if($type == 'create')

<strong>Request Ticket Description:</strong>

{{ $requestTicket->description }}

@endif

@php
    $actionText = "View Ticket";
    $color = 'primary';
@endphp

@component('mail::button', ['url' => $actionUrl, 'color' => 'primary'])
{{ $actionText }}
@endcomponent

DO NOT reply to this email. Please add a note to your request ticket if you would like to add a response.

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Thank you!')<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "If you're having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>

<hr>

<small>
{{ config('app.company_url') }}  |  {{ config('app.company_phone') }}
</small>

@endslot
@endisset
@endcomponent
