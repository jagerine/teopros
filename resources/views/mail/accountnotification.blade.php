
<img src="https://teopros.com/wp-content/uploads/2021/11/TeoPros-logo-new.png" height="100" />

<h2>Welcome!</h2>
<p>
    A new account has been created for you in the TeoPros Customer Portal.
</p>
<p>
    Please use the following link to complete your registration and track your work with TeoPros:
    {{ $link ?? '(Link missing)' }}
</p>