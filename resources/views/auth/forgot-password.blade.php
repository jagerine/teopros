<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <div class="mb-4 text-md text-gray-50 text-shadow">
            {{ __('Forgot your password? Enter your email address and we will email you a link to reset your password.') }}
        </div>

        <!-- Session Status -->
        <x-auth-session-status class="mb-2" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-2" :errors="$errors" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <label for="email" class="text-white uppercase">Email</label>
                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4 text-center">
                <button style="background-color:#156bb6;" class="rounded-full text-white px-4 py-1 shadow inline-block align-center">
                    {{ __('email link') }}
                </button>
            
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
