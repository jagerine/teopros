<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <img src="{{ asset('images/app-logo.png') }}" alt="{{ config('app.name') }}" class="w-64">
            </a>
        </x-slot>

        <h1 class="text-center text-white" style="font-size: 48px; font-family: 'Jost';">Login</h1>
        <p class="text-center text-white">Sign in to continue.</p>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <label for="email" class="uppercase text-white text-sm">{{ __('Email') }}</label>
                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <label for="password" class="uppercase text-white text-sm">{{ __('Password') }}</label>
                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-50">{{ __('Remember me') }}</span>
                </label>
            </div>
            <div class="text-center">
                <button style="background-color:#156bb6; font-family:'Jost';" class="font-jost rounded-full text-white px-4 py-1 shadow inline-block align-center">
                    {{ __('sign in') }}
                </button>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-50 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
