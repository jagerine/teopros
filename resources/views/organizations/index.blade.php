@extends('layouts.app', ['header' => 'Organizations'])

@section('content')
    <div class="p-1">
        <x-utilitymenu :items="[
            (object) ['text' => 'Add Organization', 'url' => route('organizations.create'), 'icon' => 'plus-circle'] 
        ]" />
    </div>   
    <x-datatable :data="$organizations" textFilter="1" />
@endsection