@extends('layouts.app', ['header' => $heading])

@section('content')
    
    @if(isset($organization))
        {{ Form::model($organization, ['url' => ['organizations/update/'. $organization->id]]) }}
    @else
        {{ Form::open(['url' => 'organizations/store']) }}
    @endif

        <x-forminput :params="['name' => 'name', 'placeholder' => 'Organization Name']" /> 
        <x-submitbutton />

        {{ Form::close() }}

@endsection