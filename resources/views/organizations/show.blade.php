@extends('layouts.app', ['header' => 'Organization: '.$organization->name])

@section('content')
    <x-utilitymenu :items="[
        (object) ['text' => 'Update Organization', 'url' => '/organizations/edit/'.$organization->id, 'icon' => 'pencil-alt'] , 
        (object) ['text' => 'Delete', 'url' => '/organizations/destroy/'.$organization->id, 'icon' => 'trash'] 
    ]" />

    <div class="grid grid-cols-1 gap-3 lg:grid-cols-2">
        <div><h3>Work Orders for this Organization Here</h3></div>
        <div><h3>Invoices for this Organization Here</h3></div>
    </div>
    
@endsection