@extends('layouts.app', ['header' => 'Add Request Ticket'])

@section('content')

@php $user=Auth::user(); @endphp

    {{ Form::open(['url' => '/mine/add-request']) }}

        <div class="w-full md:w-3/12 inline-block">
            <x-forminput :params="[ 'label' => 'Name', 'name' => 'name', 'value' => Auth::user()->name]" />
        </div>
        <div class="w-full md:w-3/12 inline-block">
            <x-forminput :params="[ 'label' => 'Email', 'name' => 'email',  'value' => Auth::user()->email]" />
        </div>
        <div class="clear-both"></div>
        <div class="w-full md:w-4/12 inline-block" id="CategorySelectionContainer">
            <x-forminput :params="[ 'label' => 'Category', 'placeholder' => '-', 'name' => 'category', 'type' => 'select', 'options' => [
                'RFP' => 'RFP',
                'Work Orders' => 'Work Orders',
                'Website Issue' => 'Website Issue',
                'Other' => 'Other'
            ]]" />
        </div>
        <div class="clear-both"></div>
        <div class="w-full md:w-5/12 inline-block" id="WorkOrderSelectionContainer" style="display: none;">
            <x-forminput :params="[ 'label' => 'Is this about a specific work order? (leave blank for no)', 
                    'placeholder' => '-', 'name' => 'external_id', 'type' => 'select', 'options' => []]" />
        </div>
        <div class="clear-both"></div>
        <div class="w-full md:w-6/12 inline-block">
            <x-forminput :params="[ 'label' => 'Description', 'name' => 'description', 'type' => 'textarea']" />
        </div>

    <div class="clear-both"></div>

    <x-submitbutton text="Submit Request" />

    {{ Form::close() }}

    <script>
        $(document).ready(function() {
            // alert($('#CategorySelectionContainer select').length);
            $('#CategorySelectionContainer select').change(function (e) { 
                e.preventDefault();
                let catName = $(this).val();
                if(catName == 'Work Orders') {
                    let $container = $('#WorkOrderSelectionContainer');
                    $container.show();
                    if(!$container.attr('data-loaded')){
                        let select = $container.find('select')[0];
                        $(select).attr('placeholder', 'Loading...');
                        select.options.length = 0;
                        $.getJSON('/mine/work-order-ticket-selection', function(response) {
                            select.options[select.options.length] = new Option('');
                            $(response).each(function(){
                                select.options[select.options.length] = new Option(this.name, this.id);
                            });
                            $container.attr('data-loaded', true);
                            $(select).attr('placeholder', '-');
                        });
                    }
                } else {
                    $('#WorkOrderSelectionContainer').hide();
                }
            });
        });
    </script>

@endsection