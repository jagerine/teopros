@extends('layouts.ajax')

@section('content')

@php
    $allStatuses = [];
    foreach ($hcpWorkOrders->jobs as $workOrder){
        $allStatuses[$workOrder->work_status] = $workOrder->work_status;
    }
@endphp

@if(count($allStatuses) > 0)

    <div class="my-2">
        <span class="text-400 text-xs">Show:</span> 
        <ul class="inline-block ml-2">
            @foreach($allStatuses as $status)
                <li class="inline-block mr-3 text-gray-600 text-xs">
                    <label>
                        <input type="checkbox" name="status[{{$status}}]" class="status-filter-checkbox" value="{{$status}}" checked>
                        {{ ucwords($status) }}
                    </label>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="clear-both"></div>

@endif

<table class="table-auto divide-y divide-gray-300 border-gray-300 w-full">

      <thead class="bg-gray-50 text-xs text-gray-500 text-left">
        <tr>
            <th class="px-3 py-2">Invoice</th> 
            <th class="px-3 py-2">Description</th>
            <th class="px-3 py-2">Status</th> 
        </tr>
      </thead>

      <tbody>
@foreach ($hcpWorkOrders->jobs as $workOrder)

        <tr class="work-status-row workStatus_{{ str_replace(' ','',$workOrder->work_status) }}" data-workorderstatus="{{ $workOrder->work_status }}">
            <td class="px-3 pt-3 pb-1 text-lg">{{ $workOrder->invoice_number }}</td>
            <td class="px-3 pt-3 pb-1 text-sm">
                <a href="/mine/view-work-order/{{ $workOrder->id }}" class="page-link text-lg">
                    {{ $workOrder->description }}
                </a>
            </td>
            <td class="px-3 pt-3 pb-1 text-sm">{{ ucwords($workOrder->work_status) }}</td>
        </tr>
        <tr class="border-b border-gray-300 work-status-row workStatus_{{ str_replace(' ','',$workOrder->work_status) }}" data-workorderstatus="{{ $workOrder->work_status }}">
            <td colspan="3" class="px-3 pt-0 pb-3 text-sm text-gray-600">
                <div class="flex gap-2">
                    <div class="w-6/12">
                        <span class="inline-block align-top">
                            <strong>Customer:</strong> 
                        </span>
                        <span class="inline-block align-top">
                            @if(!empty($workOrder->customer->company)) 
                                {{ $workOrder->customer->company }} <br>
                            @endif 
                            {{ $workOrder->customer->first_name }} {{ $workOrder->customer->last_name }} 
                            @if(!empty($workOrder->customer->mobile_number))
                            - 
                            <a href="tel:{{ $workOrder->customer->mobile_number }}" class="page-link">{{ $workOrder->customer->mobile_number }}</a>

                            @endif
                        </span>
                    </div>
                    <div class="w-6/12">
                        <strong>Total:</strong> ${{ $workOrder->total_amount / 100 }}<br>
                        <strong>Outstanding:</strong> ${{ $workOrder->outstanding_balance / 100 }}
                    </div>

                </div>
            </td>
        </tr>

@endforeach

        </tbody>
    </table>

@endsection