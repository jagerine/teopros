<html>
<head>
    <title></title>
    
    <style>
        html{
            margin-top: 0;
        }
        body{
            margin-top:0;
            padding-top: 3em;
            font-size: 11pt;
            font-family: sans-serif;
        }
        pre {
            font-family: sans-serif;
            white-space: pre-wrap;
        }
        .rowdiv{
            display: inline-block;
            vertical-align: text-top;
            float: left;
        }
        .hr{
            border: 1px solid #777;
            margin: 20px 0;
            clear: both;
        }
    </style>
</head> 
<body>
    <h2 style="margin-top: 0; padding-top:0;">Work Order</h2>
    
    <table style="border-bottom: 0px; width:100%;">
        @php
            $fields = [
                'Customer' => $workOrderDetails->customer->company, 
                'Project' => $workOrderDetails->description, 
                'Invoice Number' => $workOrderDetails->invoice_number,
                'Work Status' => ucwords($workOrderDetails->work_status),
                'Notes' => $workOrderDetails->note
            ];
            
        @endphp

        @foreach($fields as $key => $value)
            
            <tr style="border-bottom:0px;">
                <th style="border-bottom:0px; text-align:left; vertical-align:top;">{{ $key }}</th>
                <td style="border-bottom:0px; vertical-align:top;">{{ $value }}</td>
            </tr>
        @endforeach

        <tr style="border-bottom:0px;">
            <th style="border-bottom:0px; text-align:left; vertical-align:top;">Address</th>
            <td style="border-bottom:0px; vertical-align:top;">{{ $workOrderDetails->address->street }} {{ $workOrderDetails->address->street_line_2 }} <br>
                {{ $workOrderDetails->address->city }}, {{ $workOrderDetails->address->state }} {{ $workOrderDetails->address->zip }} </td>
        </tr>
        
    </table>

    <div class="hr"></div>
    <h2 style="margin-top: 0; padding-top:0;">Work Details</h2>
    <div class="">


        @foreach ($workOrderLineItems->data as $workOrderLineItem)

            <div class="rowdiv" style="width: 15%;">
                <strong>{{ ucwords($workOrderLineItem->kind) }}</strong>
            </div>
            <div class="rowdiv" style="width: 40%;">
                <strong>{{ ucwords($workOrderLineItem->name) }} </strong>
            </div>
            <div class="rowdiv" style="width: 25%; text-align:right;">
                Cost: <strong>${{ ucwords($workOrderLineItem->unit_price) / 100 }}</strong>
            </div>
            <div class="rowdiv" style="width: 20%; text-align:right;">
                Qty: <strong>{{ $workOrderLineItem->quantity }} 
            </div>
            <div style="clear: both;"></div>
            <div>
                <pre>{{ $workOrderLineItem->description }}</pre>
            </div>
            
            <div class="hr"></div>

        @endforeach

    </div>


</body> 
</html>