@extends('layouts.app', ['header' => 'Work Order Details'])

@section('content')

<x-utilitymenu :items="[
        (object) ['text' => 'New Request Ticket', 'url' => '/mine/addrequest', 'icon' => 'plus-circle'] , 
        (object) ['text' => 'PDF Version', 'url' => '/mine/view-work-order-pdf'.$workOrderId, 'icon' => 'printer', 'id' => 'MakePdfFormSubmit'] 
    ]" />

{{ Form::open(['url' => '/mine/view-work-order-pdf/'.$workOrderId, 'method' => 'GET', 'id' => 'MakePdfForm']) }}

<input type="hidden" name="DetailsKey" id="WorkOrderDetailsKeyInput" />
<input type="hidden" name="LineItemsKey" id="WorkOrderLineItemsKeyInput" />

{{ Form::close() }}

<x-subheading text="General Information" />
<div id="WorkOrderDetailsContainer">
    <x-loadspinner /> 
</div>

<hr class="my-10 mx-2" />

<x-subheading text="Line Items" />
<div id="WorkOrderLineItemsContainer">
    <x-loadspinner /> 
</div>
   
<div class="flex gap-6 w-full">
    <div class="w-8/12">
        <x-subheading />
        <div id="WorkOrdersContainer"></div>
    </div>
    <div class="w-4/12">
        <x-subheading />
        <div id="TicketsContainer"></div>
    </div>
</div>

    {{-- /mine/work-orders goes to MineController::workOrders --}}    
    <script>
        $(document).ready(function() {
            $.ajax({
                url: '/mine/view-work-order-details/{{ $workOrderId }}',
                success: function (response) {
                    $('#WorkOrderDetailsKeyInput').val(response.storedResponseKey);
                    $('#WorkOrderDetailsContainer').html(response.html);
                }
            });

            $.ajax({
                url: '/mine/view-work-order-line-items/{{ $workOrderId }}',
                success: function (response) {
                    $('#WorkOrderLineItemsKeyInput').val(response.storedResponseKey);
                    $('#WorkOrderLineItemsContainer').html(response.html);
                }
            }); 

            $('#MakePdfFormSubmit').click(function(e) {
                e.preventDefault();
                $('#MakePdfForm').submit();
                return false;
            });
        });
    </script>


    <style type="text/css">
        /* Add wrap styling since work order description comes over as preformatted text */
        pre {
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            word-wrap: break-word;
        }
    </style>

@endsection