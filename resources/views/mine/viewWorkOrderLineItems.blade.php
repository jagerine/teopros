@extends('layouts.ajax')

@section('content')

    <div class="">
        @foreach ($workOrderLineItems->data as $workOrderLineItem)
            
        <div class="flex gap-4 border-b border-t border-gray-200 mb-6 py-5 px-3 border-l-4 rounded-l-md border-l-solid">
            <div class="w-2/12 md:w-1/12">
                <div>
                    <div class="text-right text-sm md:text-lg text-blue-900 font-bold truncate">
                        {{ ucwords($workOrderLineItem->kind) }}
                    </div>
                </div>
            </div>
            <div class="w-8/12 md:w-9/12">
                <div class="">
                    <div>
                        <strong>{{ $workOrderLineItem->name }}</strong> <br>
                        <pre class="text-xs text-gray-800 text-left py-2">{{ $workOrderLineItem->description }}</pre>
                    </div>
                </div>
            </div>
            <div class="w-2/12">
                <div class="text-right bg-gray-50 h-full p-4 text-sm rounded-r-lg">
                    <div class="">
                        Cost: <strong class="inline-block md:w-16">${{ ucwords($workOrderLineItem->unit_price) / 100 }}</strong> <br>
                        <div class="m-2 md:invisible md:hidden md:display-none"></div>
                        Qty: <strong class="inline-block md:w-16">{{ $workOrderLineItem->quantity }} </strong>
                    </div>
                </div>
            </div>
        </div>

        @endforeach

    </div>

@endsection