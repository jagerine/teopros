@extends('layouts.app', ['header' => 'Dashboard'])

@section('content')

<x-utilitymenu :items="[
        (object) ['text' => 'New Request Ticket', 'url' => '/mine/add-request', 'icon' => 'plus-circle'] 
    ]" />
   
<div class="flex gap-6 w-full flex-col-reverse lg:flex-row p-2 lg:p-0.5">
    <div class="w-full lg:w-7/12">
        <x-subheading text="Work Orders" />
        <div id="WorkOrdersContainer">
            <x-loadspinner />
        </div>
    </div>
    <div class="w-full lg:w-5/12">
        <x-subheading text="Request Tickets" />
        <div id="TicketsContainer" class="m-2">
            
            @foreach ($requestTickets as $requestTicket)
            
                <div class="pb-4 border-b border-gray-500 border-dotted mb-4">
                    <small class="text-gray-600">
                        Ticket #{{ $requestTicket->id }}, 
                        Updated {{ $requestTicket->updated_at->diffForHumans() }}

                    </small>
                    <small class="text-gray-600 float-right">
                        {{ $requestTicket->status }}
                    </small>
                    <h4 class="whitespace-nowrap truncate elipsis">
                        <a href="/tickets/view/{{ $requestTicket->id }}" class="page-link">
                            {{ $requestTicket->description }}
                        </a>
                    </h4>
                </div>
                
            @endforeach

        </div>
    </div>
</div>

    {{-- /mine/work-orders goes to MineController::workOrders --}}    
    <script>

        function applyWorkOrderStatusView()
        {
            let selectedStatuses = [];
            $('.status-filter-checkbox').each(function() {
                if ($(this).is(':checked')) {
                    selectedStatuses.push($(this).val());
                }
            });
            $('.work-status-row').each(function(){
                let rowStatus = $(this).attr('data-workorderstatus');
                if($.inArray(rowStatus, selectedStatuses) == -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });

        }

        $(document).ready(function() {
            $('#WorkOrdersContainer')
                .load('/mine/work-orders', function(){
                    $('.status-filter-checkbox').each(function() {
                        if( this.value.indexOf('complete') > -1 ) {
                            $(this).prop('checked', false);
                        }
                    });
                    applyWorkOrderStatusView();
                    $('.status-filter-checkbox').click(function() {
                        applyWorkOrderStatusView();
                    });
                }); 
            
                

            
        });
    </script>

@endsection