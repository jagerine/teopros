
    @php($authUser = Auth::user())

    @foreach ($historicalNotes as $historicalNote)
                
        <div class="flex flex-row px-2 py-4 border-b bg-gray-50 mb-5 rounded-md" id="HistoricalNote_{{ $historicalNote->id }}">
            <div class="w-2/12 lg:w-1/12 text-left">
                <span class="bg-gray-200 p-2 m-2 rounded-md">
                    <x-icon type="chat-alt" fill="1" />
                </span>
            </div>
            <div class="w-10/12 lg:w-11/12 text-left pr-4">
                
                @if($authUser->account_type == 'Admin' || is_null($authUser->account_type) || $authUser->id == $historicalNote->created_by)
                    <span class="float-right text-sm text-gray-400">
                        <span class="comment-delete-button cursor-pointer" data-historicalnoteid="{{ $historicalNote->id }}">
                            <x-icon type="trash" fill="1" />
                        </span>
                    </span>
                @endif

                <small class="block mb-2 text-gray-700">
                    From {{ $historicalNote->user()->name }}, 
                    {{ $historicalNote->created_at->diffForHumans() }}
                </small>
                <div class="pr-4">
                    {{ $historicalNote->note }}
                </div>
            </div>
        </div>  

    @endforeach

    <script>
        $(document).ready(function(){
            $('.comment-delete-button').click(function(){
                if(!confirm('Are you sure you want to delete this note?')){
                    return false;
                }
                var historicalNoteId = $(this).data('historicalnoteid');
                var url = '/app/delete-historical-note/' + historicalNoteId;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function(data){
                        if(data.success){
                            $('#HistoricalNote_' + historicalNoteId).fadeOut(function(){
                                $(this).remove();
                            });
                        }
                    }
                });
            });
        });
    </script>