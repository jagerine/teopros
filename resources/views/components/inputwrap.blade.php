
        <div class="relative pb-2 pt-2 mr-1">

            @if (!empty($label)) 
            
            <label class="text-base leading-7 text-blueGray-500 block lg:inline-block w-1/6">{{ $label }}</label>
            @endif

            {{ $input }}
    
        </div>