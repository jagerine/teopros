
    <!-- Medium -->
    {{ @foreach ($actions as $action)
        
    @endforeach}}


<div class="flex items-center justify-center mb-4">

    @foreach ($actions as $action)

    <form action=" {{$action->destination}}">
        <button
            class="bg-purple-500 text-white hover:bg-purple-700 hover:text-white active:bg-purple-700 font-bold uppercase text-sm px-6 py-3 rounded-l outline-none focus:outline-none mb-1 ease-linear transition-all duration-150"
            type="button">
            <i class="fas {{$action->icon}}"></i> {{$action->label}}
        </button>
    </form>
    @endforeach

    
    <button
        class="bg-purple-500 text-white hover:bg-purple-700 hover:text-white active:bg-purple-700 font-bold uppercase text-sm px-6 py-3 outline-none focus:outline-none mb-1 ease-linear transition-all duration-150"
        type="button">
        <i class="fas fa-heart"></i>
    </button>
    <button
        class="bg-purple-500 text-white hover:bg-purple-700 hover:text-white active:bg-purple-700 font-bold uppercase text-sm px-6 py-3 rounded-r outline-none focus:outline-none mb-1 ease-linear transition-all duration-150"
        type="button">
        <i class="fas fa-wrench"></i>
    </button>
</div>

