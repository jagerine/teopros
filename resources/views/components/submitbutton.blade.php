
    <div class="border-t mt-5 border-gray-200 pt-3 inline-block w-9/12">
        {{ Form::submit($text, ['name' => 'submit', 'class' => 'inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500']) }}
    </div>