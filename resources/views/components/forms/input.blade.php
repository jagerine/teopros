
<div class="relative pb-2 pt-2 mr-1">

    @if (!empty($params['label'])) 
    
    <label for={{ $params['name'] }} class="pl-1 text-xs text-gray-500 block">{{ $params['label'] }}</label>
    @else
        @php
            $params['label'] = 'false';
        @endphp

    @endif


    @switch($params['type'])    
        @case('date')
        {{ Form::text($params['name'], null, ['data-label' => $params['label'], 'placeholder' => $params['placeholder'], 'class' => 'bg-gray-100 border-gray-200 rounded-lg focus:border-gray-500 w-full mr-1 md:min-w-1/4']) }}
            @break
        @case('select')
            {{ Form::select($params['name'], $params['options'], $params['default'] ?? null , ['placeholder' => $params['placeholder'] ?? null, 'data-label' => $params['label'], 'class'=> 'bg-gray-100 border-gray-200 rounded-lg focus:border-gray-500 w-full md:min-w-1/4']) }}
            @break    
        @case('textarea')
        @case('longtext')
            {{ Form::textArea($params['name'], null, ['data-label' => $params['label'], 'placeholder' => $params['placeholder'], 'class' => 'h-32 bg-gray-100 border-gray-200 rounded-lg focus:border-gray-500 w-full']) }}
            @break
        @case('checkbox')
            {{ Form::checkbox($params['name'], 1) }}
            @break
        @case('password')
            <input data-label="Password" placeholder="" class="bg-gray-100 border-gray-200 rounded-lg focus:border-gray-500 w-full" name="password" type="password" value="{{ $params['value'] ?? '' }}">
            @break  
        @case('text')
        @default
            {{ Form::text($params['name'], $params['value'] ?? null, ['data-label' => $params['label'], 'placeholder' => $params['placeholder'], 'class' => 'bg-gray-100 border-gray-200 rounded-lg focus:border-gray-500 w-full mr-1 md:min-w-1/4']) }}
    @endswitch


</div>