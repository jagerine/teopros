@props(['status'])

@if ($status)
<div class=" bg-gray-50 opacity-90 rounded-md p-2 mb-2">
    <div {{ $attributes->merge(['class' => 'font-medium text-sm text-green-600']) }}>
        {{ $status }}
    </div>
</div>
@endif
