
        <span class="icon-link">
            <a href="{{ $url }}" id="{{ $id }}" >
                <x-icon :type="$icon" :fill="$fill" />
                
                {{ $text }}
            
            </a>
        </span>