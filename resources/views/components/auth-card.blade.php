<div class="flex flex-col md:flex-row pt-6 sm:pt-0">
    <div class="w-full md:w-3/12 lg:w-4/12 text-center lg:px-5">
        <a href="/">
            <img src="{{ asset('images/app-logo.png') }}" alt="TeoPros Plumbing, Heating, Electrical, and Air" class="w-full max-w-xs mt-6 inline-block align-text-top">
        </a>
    </div>

    <div class="w-full md:w-6/12 lg:w-4/12 mt-6 px-6 py-6 lg:px-8 xl:px-16 shadow-md overflow-hidden bluey">
        {{ $slot }}
    </div>

    <div class="w-full md:w-3/12 lg:w-4/12 text-center">
        <img src="{{ asset('images/wjhp.png') }}" alt="We just help people" style="transform: rotate(347deg);" class="w-full max-w-sm inline-block align-text-top mt-4">
    </div>
</div>
