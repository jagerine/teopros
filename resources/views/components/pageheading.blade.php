        <h2 class="text-lg font-bold leading-7 text-blue-900 sm:text-1xl sm:truncate border-b border-gray-100">
            @if (!empty($icon)) 
                <x-icon :type="$icon" fill="1"  />
            @endif 
            {{ $text }}
        </h2>