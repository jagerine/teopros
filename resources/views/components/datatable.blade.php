    

            <table class="table-auto divide-y divide-gray-300 border-gray-300 w-full">
              
              @if ($data->headings)

                <thead class="bg-gray-50">
                  <tr>
                    <th class="px-6 py-2 text-xs text-gray-500 left"></th> 
                    @foreach ($data->headings as $heading)

                    <th class="px-6 py-2 text-xs text-gray-500 text-left">{{ $heading }}</th> 
                    @endforeach

                  </tr>
                </thead>
                @endif

                <tbody class="bg-white divide-y divide-gray-300">
                  @if (empty($data->rows))
                    <tr>
                      <td></td>
                      <td><em class="text-gray-400">None</em></td>
                    </tr>
                  @endif

                    
                    @foreach ($data->rows as $values)
                      
                    <tr class="">
                        <td class="dotmenutd">
                          @if (!empty($values['_actions']))
                          <x-actionmenu :items="$values['_actions']" />
                          @endif 
                        </td>
                        @foreach ($values as $valueKey => $value)
                          @if ($valueKey == '_actions' || $valueKey == '_links') 
                            @continue 
                          @endif

                        <td class="px-3 py-3 text-sm" data-key="{{ $valueKey }}">
                          @if ($valueKey == 'created_at' || $valueKey == 'updated_at')
                            <span
                              x-data="{date: new Date($el.innerText)}"
                              x-text="date.toLocaleString().toLowerCase()"
                              >{{ $value }}</span> 
                          @else
                            @if (!empty($values['_links'][$valueKey]))
                              <a 
                                href="{{ $values['_links'][$valueKey] }}" 
                                class="page-link" 
                                target="{{ (stripos($values['_links'][$valueKey], 'http') !== false) ? 'blank' : '' }}">
                            @endif
                            {{ $value }}
                            @if (!empty($values['_links'][$valueKey]))
                              </a>
                            @endif
                          @endif

                        </td>
                        @endforeach

                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
 