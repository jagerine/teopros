
    <div x-data="{dropdownMenu: false}" class="action-menu relative" 
        @open-dropdown.window="if ($event.detail.id == 1) dropdownMenu = true"
            @click.away="dropdownMenu = false">
        
        <button @click="dropdownMenu = ! dropdownMenu" class="ml-2 border-gray-300 border rounded-full h-7 w-7 text-center align-middle inline-block">
            <span class="text-xs text-gray-600 inline-block w-6 mx-auto">
                <x-icon type="chevron-down" fill="1" />
            </span>
        </button>
        <div x-show="dropdownMenu" class="absolute left-3 top-7 py-z mt-2 bg-white bg-gray-100 rounded-md shadow-md  w-44 z-10">
            @foreach ($items as $item)
                <a 
                    @if ($item->text == 'Delete') 
                        onclick="return confirm('Are you sure you want to delete this?')"
                    @endif
                    href="{{ $item->url }}" 
                    class="block px-4 py-2 text-xs text-gray-300 text-gray-700 rounded-md  hover:bg-gray-300 hover:text-white">
                    <x-icon type="{{ $item->icon }}" fill="1" color="text-gray-500" />
                    {{ $item->text }}
                </a>
            @endforeach

        </div>
    </div> 