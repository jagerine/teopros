    
    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline-block" fill="{{ $fill }}" viewBox="0 0 24 24" stroke="{{ $stroke }}">
        <path fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="{{ $type }}" class="{{ $color }}" />
    </svg>  