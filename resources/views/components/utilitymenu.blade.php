<div class="utility-menu mb-4 border-b border-gray-100 pb-4 pt-2 md:pt-0">
    @foreach ($items as $item)
    
    <span class="bg-blue-50 py-1 px-3 rounded-2xl border border-gray-200 text-gray-500 hover:text-yellow-700 hover:bg-blue-100 mr-1">
        <x-iconlink :icon="$item->icon" :text="$item->text" :url="$item->url" fill=1, :id="$item->id ?? ''" />
    </span>

    @endforeach
</div>