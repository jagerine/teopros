@extends('layouts.app', ['header' => 'Request Ticket #'.$requestTicket->id])

@section('content')

@php
    $utilityMenuItems = [
        (object) ['text' => 'Add Note', 'url' => '/tickets/add-note/'.$requestTicket->id, 'icon' => 'plus-circle'] 
    ];
    if($authUser->isAdmin()){
        $utilityMenuItems[] = (object) ['text' => 'Set Status', 'url' => '/tickets/set-status/'.$requestTicket->id, 'icon' => 'tag'];
    }
@endphp

<x-utilitymenu :items="$utilityMenuItems" />

<div id="SetTicketStatusForm" style="display: none;" class="mb-8 ticket-form">
    <x-subheading text="Set Ticket Status #{{$requestTicket->id}}" />

        {{ Form::open(['url' => '/tickets/set-status/'.$requestTicket->id]) }}
        
        <div class="flex flex-row">
            <div class="m-2 mb-0 w-11/12 md:w-7/12 lg:w-5/12">
                <x-forminput :params="[
                    'type' => 'select',
                    'name' => 'status',
                    'label' => 'Status', 
                    'options' => $ticketStatuses
                ]" /> 
            </div>
            <div class="w-1/12 pt-4 text-xl form-closer text-gray-400">
                <span><x-icon type="x-circle" fill="1" /></span>
            </div>
        </div>
    
        <x-submitbutton />
        
        {{ Form::close() }}
        <div class="m-6"></div>

</div>

<div id="AddTicketNoteForm" style="display: none;" class="mb-8 ticket-form">
    <x-subheading text="Add Note to Ticket #{{$requestTicket->id}}" />

    {{ Form::open(['url' => '/tickets/add-note/'.$requestTicket->id]) }}
    
    <div class="flex flex-row">
        <div class="m-2 mb-0 w-11/12">
            <x-forminput :params="[
                'type' => 'textarea',
                'name' => 'note',
                'label' => 'Note'
            ]" /> 
        </div>
        <div class="w-1/12 pt-4 text-xl form-closer text-gray-400">
            <span><x-icon type="x-circle" fill="1" /></span>
        </div>
    </div>

    <x-submitbutton />
    
    {{ Form::close() }}
    <div class="m-6"></div>
</div>

<x-subheading text="Details" />

    <div class="">

        @include('tickets.maininfo')

    </div>

    <div class="clear-both m-8"></div>

    <div class="">
        <x-subheading text="Description" />
        <div class="text-md my-2 mx-1">
            
            {!! nl2br($requestTicket->description) !!}
            
        </div>
    </div>

    <div class="clear-both m-8"></div>
    <div class="">
        <x-subheading text="History" />
        
        @foreach ($historicalNotes as $historicalNote)
            
            <div class="flex flex-row px-2 py-4 border-b bg-gray-50 mb-5" id="HistoricalNote_{{ $historicalNote->id }}">
                <div class="w-2/12 lg:w-1/12 text-left">
                    <span class="bg-gray-200 p-2 m-2 rounded-md">
                        <x-icon type="chat-alt" fill="1" />
                    </span>
                </div>
                <div class="w-10/12 lg:w-11/12 text-left pr-4">
                    
                    @if($authUser->account_type == 'Admin' || is_null($authUser->account_type) || $authUser->id == $historicalNote->created_by)
                        <span class="float-right text-sm text-gray-400">
                            <span class="comment-delete-button cursor-pointer" data-historicalnoteid="{{ $historicalNote->id }}">
                                <x-icon type="trash" fill="1" />
                            </span>
                        </span>
                    @endif

                    <small class="block mb-2 text-gray-700">
                        From {{ $historicalNote->user()->name }}, 
                        {{ $historicalNote->created_at->diffForHumans() }}
                    </small>
                    <div class="pr-4">
                        {{ $historicalNote->note }}
                    </div>
                </div>
            </div>  

        @endforeach

    </div>
    <script>
        $(document).ready(function(){

            $('.utility-menu').find('a:contains(Set Status)').click(function(e){
                e.preventDefault();
                $('.ticket-form:visible').slideUp();
                $('#SetTicketStatusForm').slideDown();
                return false;
            });
            $('.utility-menu').find('a:contains(Add Note)').click(function(e){
                e.preventDefault();
                $('.ticket-form:visible').slideUp();
                $('#AddTicketNoteForm').slideDown();
                return false;
            });
            $('.form-closer span').click(function(e){
                e.preventDefault();
                $(this).parents('.ticket-form').slideUp();
                return false;
            }).css('cursor', 'pointer');

            $('#RequestTicketExternalId').each(function(){
                $.ajax({
                    'url' : '/tickets/get-work-order-name/{{ $requestTicket->external_id }}',
                    'success' : function(response){
                        $('#RequestTicketExternalId').html(response);
                    }
                });
            });

        });
    </script>
    

@endsection