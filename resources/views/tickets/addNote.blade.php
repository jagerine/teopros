@extends('layouts.app', ['header' => 'Add Note to Ticket #'.$requestTicket->id])

@section('content')

<x-utilitymenu :items="[
        (object) ['text' => 'Add Note', 'url' => '/tickets/add-note/'.$requestTicket->id, 'icon' => 'plus-circle', 'id' => 'AddTicketsNote'] , 
        //(object) ['text' => 'Delete', 'url' => '/organizations/destroy/'.$requestTicket->id, 'icon' => 'trash'] 
    ]" />



<div id="AddTicketNoteForm" style="display: none;">
    <x-subheading text="Add Note to Ticket #{{$requestTicket->id}}" />

    {{ Form::open(['url' => '/tickets/add-note/'.$requestTicket->id]) }}
    
    <x-forminput :params="[
        'type' => 'textarea',
        'name' => 'note',
        'label' => 'Note'
    ]" /> 

    <x-submitbutton />
    {{ Form::close() }}

</div>

<x-subheading text="Details" />
    <div class="">

        @include('tickets.maininfo')

    </div>

    <div class="clear-both m-8"></div>

    <div class="">
        <x-subheading text="Description" />
        <div class="text-md my-2 mx-1">
            {!! nl2br($requestTicket->description) !!}
        </div>
    </div>

    <div class="clear-both m-8"></div>
    <div class="">
        <x-subheading text="History" />
        
        @foreach ($historicalNotes as $historicalNote)
            
            <div class="flex items-center px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b bg-gray-50">
                <div class="flex-shrink-0">
                    <div class="flex items-center justify-center h-12 w-12 rounded-md bg-gray-100">
                        <svg class="h-6 w-6 text-gray-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z" />
                        </svg>
                    </div>
                </div>
                <div class="mt-2 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {{ $historicalNote->note }}
                </div>
            </div>  

        @endforeach

    </div>
    <script>
        $(document).ready(function(){
            console.log('asdf');
            //  $('.utility-menu').find('a:contains(Add Note)').
            /*alert('dsf');
            alert($('.utility-menu').find("a:contains('Add Note')").length);
            $('.utility-menu').find("a:contains('Add Note')").click(function(e){
                e.preventDefault();
                $('#AddTicketsNoteForm').slideDown();
                return false;
            });*/
        });
    </script>

@endsection