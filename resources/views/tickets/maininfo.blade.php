

        @php
            $fields = [
                'Ticket#' => $requestTicket->id, 
                'Last Update' => $requestTicket->updated_at->diffForHumans(),
                'Status' => ucwords($requestTicket->status),
            ];
            
        @endphp
        
        <dl>
            @foreach ($fields as $key => $value)
                
            <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b bg-gray-50">
                <dt class="text-sm font-medium text-gray-500">
                    {{ $key }}
                </dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {{ $value }}
                </dd>
            </div>
    
            @endforeach

            @if(!empty($requestTicket->external_id))
            
            <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b bg-gray-50">
                <dt class="text-sm font-medium text-gray-500">
                    Work Order#
                </dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    <span id="RequestTicketExternalId" data-externalid="{{ $requestTicket->external_id }}">{{ $requestTicket->external_id }}</span>
                </dd>
            </div>
            
            @endif

        </dl>