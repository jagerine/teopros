@extends('layouts.app', ['header' => 'Request Tickets'])

@section('content')

<div class="flex flex-row w-full gap-2">
    <div class="w-full md:w-3/12 lg:1/6 bg-gray-50 p-1.5">

        {{ Form::open(['method' => 'GET', 'id' => 'TicketFilterForm']) }}

        <strong>
            <x-icon type="filter" />
            Filters
        </strong>

        <div class="m-1 mt-2">
            <small class="text-gray-500 uppercase" style="font-size: x-small;">By Status</small>
            @php
                $selectedStatuses = request()->query('status') ?? ['Open', 'In Progress'];
                $count = 0;
            @endphp
            @foreach ($statusOptions as $status) 
                @php($checked = in_array($status, $selectedStatuses) ? 'checked' : '')

            <div class="text-sm text-gray-800 mt-1.5">
                <label for="StatusCheckbox-{{ $status }}">
                    <input class="mr-2 status-filter-checkbox" type="checkbox" id="StatusCheckbox-{{ $status }}" name="status[{{ $count }}]" value="{{ $status }}" {{ $checked }} />
                    {{ $status }}
                </label>
            </div>
                @php($count++)
            @endforeach

            <hr class="my-5">
            <small class="text-gray-500 uppercase" style="font-size: x-small;">By Customer</small>
            <x-forminput :params="[
                'name' => 'customer',
                'type' => 'select',
                'options' => $users,
                'default' => request()->query('customer') ?? null,
            ]" />

            <div id="ApplyFiltersButton">
                <x-submitbutton text="Apply Filters" /><br>
                <a href="javascript:void(0)" id="FilterReset" class="page-link text-xs mt-4 ml-1 inline-block">Reset Filters</a>
            </div>
        </div>

        {{ Form::close() }}

    </div>
    <div class="w-full md:w-9/12 lg:5/6">
        <x-datatable :data="$requestTickets" />
    </div>
</div>

<script>
    $(document).ready(function() {
       $('#FilterReset').click(function() {
           window.location = '/tickets';
       });
    });
    
</script>

@endsection