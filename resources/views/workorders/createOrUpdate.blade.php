@extends('layouts.app', ['header' => $heading])

@section('content')
    
    @if(isset($workorder))
        {{ Form::model($workorder, ['url' => ['/workorders/update/'. $workorder->id]]) }}
    @else
        {{ Form::open(['url' => '/workorders/store']) }}
    @endif
        <x-forminput :params="['type' => 'select', 'name' => 'work_type', 'placeholder' => 'Type', 'options' => ['Improvement' => 'Improvement', 'Repair' => 'Repair']]" />
        <x-forminput :params="['name' => 'project_name', 'placeholder' => 'Project Name']" /> 
        <x-forminput :params="['type' => 'longtext', 'name' => 'description', 'placeholder' => 'Description']" /> 
        <x-forminput :params="['type' => 'text', 'name' => 'location', 'placeholder' => 'Location']" /> 
        <hr class="mt-4 mb-4">

        <div class="grid grid-cols-1 md:grid-cols-4">
            <div><x-forminput :params="['name' => 'monetary_limit', 'placeholder' => 'Monetary Limit']" /> </div>
            <div><x-forminput :params="['type' => 'date', 'name' => 'complete_by', 'placeholder' => 'Complete By']" /> </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-4">
            <div><x-forminput :params="['type' => 'date', 'name' => 'service_start', 'placeholder' => 'Service Start']" /> </div>
            <div><x-forminput :params="['type' => 'date', 'name' => 'service_end', 'placeholder' => 'Service End']" /> </div>
        </div>

        <hr class="mt-4 mb-4">

        <div class="grid grid-cols-1 md:grid-cols-4">
            <div><x-forminput :params="['type' => 'text', 'name' => 'contact_name', 'placeholder' => 'Contact Name']" /> </div>
            <div><x-forminput :params="['type' => 'email', 'name' => 'contact_email', 'placeholder' => 'Contact Email']" /> </div>
            <div><x-forminput :params="['type' => 'phone', 'name' => 'contact_phone', 'placeholder' => 'Contact Phone']" /> </div>
        </div>
        
        <x-submitbutton />

        {{ Form::close() }}

@endsection