@extends('layouts.app', ['header' => 'Work Order: '.$workorder->project_name])

@section('content')
    <x-utilitymenu :items="[
        (object) ['text' => 'Update Work Order', 'url' => '/organizations/edit/'.$workorder->id, 'icon' => 'pencil-alt'] , 
        (object) ['text' => 'Delete', 'url' => '/organizations/destroy/'.$workorder->id, 'icon' => 'trash'] 
    ]" />

    <div class="grid grid-cols-1 gap-3 lg:grid-cols-2">
        


    </div>
    
@endsection