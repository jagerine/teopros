@if (Request::ajax()) 

    @include('layouts.ajax')

@else

    @include('layouts.app-default')

@endif