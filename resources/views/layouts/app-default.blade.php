<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'TeoPros Portal') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
        </script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="px-0 py-1 md:p-2">
                <div class="max-w-7xl mx-auto py-2 px-4 sm:px-6 lg:px-8">

                    <div class="flex flex-col-reverse md:flex-row">
                        <div class="text-2xl text-yellow-600 md:flex-grow">
                            {{ $header }}
                            
                        </div>
                        <div class="md:flex-grow-0 w-full md:w-2/6">
                            <div id="SearchForm" class="relative">
                                {{ Form::open(['url' => '/search', 'method' => 'GET']) }}
                                    <x-input value="{{ request()->all()['app-search'] ?? '' }}" name="app-search" type="text" placeholder="Search" class="w-full pr-9" />
                                    <button class="bg-transparent border-0 absolute top-2 right-2 text-gray-400"><x-icon fill="1" type="search" /></button>   
                                {{ Form::close() }}
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </header>

            <!-- Page Content -->
            <main>
                <div class="py-1wq pb-10">
                    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        <div class="bg-white shadow-sm sm:rounded-lg">
                            <div class="p-1 md:p-3 lg:p-6 bg-white border-b border-gray-200">
                                @if ($errors->all())
                                <div class="bg-yellow-50 border-l-4 border-yellow-500 text-yellow-700 p-4 rounded-md mb-8" role="alert">
                                    @foreach ($errors->all() as $errors)
                                            <p>{{ $errors }}</p>
                                    @endforeach
                                </div>
                                @endif

                                @if(Session::has('message'))
                                <div x-data="{ show: true }" x-show="show" x-init="setTimeout(() => show = false, 3000)" class="bg-green-50 border-l-4 border-green-500 text-green-700 p-4 rounded-md mb-8" role="alert">
                                    <p>
                                        <x-icon type="check-circle" /> 
                                        {{ Session::get('message') }}</p>
                                </div>
                                @endif

                                @yield('content')

                            </div>
                        </div>
                    </div>
                </div>

            </main>

            <div class="footer">
                <div class="max-w-7xl mx-auto py-10 px-4 sm:px-6 lg:px-8 text-center text-xs text-gray-400">
                    Copyright &copy; {{ date('Y') }} 
                    &nbsp; &nbsp; | &nbsp; &nbsp;
                    All data contained herein is confidential and proprietary.
                </div>
            </div>
        </div>
    </body>
</html>
