@extends('layouts.app', ['header' => 'Admin Dashboard'])

@section('content')

     
    <x-utilitymenu :items="[
        (object) ['text' => 'New User Account', 'url' => route('app.addaccount'), 'icon' => 'plus-circle'] 
    ]" />

    <div class="flex flex-col md:flex-row gap-4">
        <div class="md:w-8/12 text-xs md:text-sm">
            <x-subheading text="Request Tickets" />

            <div id="TicketsContainer" class="m-2 text-xs">
                
                <table class="w-full">
                    <!-- <thead>
                        <tr>
                            <th class="text-left">Customer</th>
                            <th class="text-left">Opened</th>
                            <th class="text-left">Last Updated</th>
                            <th class="text-left">Status</th>
                        </tr>
                    </thead>-->
                    <tbody>
                @foreach ($requestTickets as $requestTicket)
                    @php
                        $descr = $requestTicket->description;
                        if(strlen($descr) > 90) {
                            $descr = substr($descr, 0, 90) . '...';
                        }
                    @endphp
                        <tr class="">
                            <td class="text-left p-1 pb-0">
                                <span class="text-gray-400 block">Customer</span>
                                <strong>{{ $requestTicket->external_name }}</strong>
                            </td>
                            <td class="text-left p-1 pb-0"><span class="text-gray-400 block">Opened:</span> 
                                {{ $requestTicket->created_at->format('m/d/Y') }}</td>
                            <td class="text-left p-1 pb-0">
                                <span class="text-gray-400 block">Last Updated:</span> 
                                {{ $requestTicket->updated_at->diffForHumans() }}</td>
                            <td class="text-left p-1 pb-0">
                                <span class="text-gray-400 block">Status:</span> 
                                {{ $requestTicket->status }}</td>
                        </tr>
                        <tr class="mb-4 pb-4 border-gray-200 border-b">
                            <td colspan="4" class="p-1 pb-2 text-gray-700">
                                <a href="/tickets/view/{{ $requestTicket->id }}" class="page-link">{{ $descr }}</a>
                            </td>
                        </tr>
                @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class="md:w-4/12 text-xs md:text-xs" style="font-size: x-small;">
            <x-subheading text="Users" />
            
            <div class="max-h-96 overflow-y-auto">
            
            @foreach ($users as $user)
                

                <div class="bg-gray-50 mb-2 p-1 pl-2 rounded-md">
                    <div class="flex flex-row gap-2">
                        <div class="w-7/12">
                            <span style="font-size: smaller;" class="text-gray-300"><x-icon type="user" /></span>
                            <a href="/app/user/{{ $user->id }}" class="page-link">
                                {{ $user->name }}
                            </a>
                        </div>
                        <div class="w-5/12">
                            {{ $user->account_type }}
                        </div>
                        
                    </div>
                    <div class="clear-both"></div>
                    <div class="flex flex-row gap-2">
                        
                        <div class="w-7/12 truncate elipsis">
                            <a class="page-link" href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                        </div>
                        <div class="w-5/12 truncate elipsis">
                            {{ $user->external_name }}
                        </div>
                    </div>
                </div>

            @endforeach
            
            </div>
            
        </div>
    </div>
     
    


@endsection