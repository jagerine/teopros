@extends('layouts.app', ['header' => 'User Profile'])

@section('content')


<script>
    $(document).ready(function(){
        $('.utility-menu').find('a:contains(Add Note)').click(function(e){
                e.preventDefault();
                $('#AddUserNoteForm').slideDown();
                return false;
            });
            $('.form-closer span').click(function(e){
                e.preventDefault();
                $(this).parents('.user-note-form').slideUp();
                return false;
            }).css('cursor', 'pointer');

        $("a:contains('activate Account')").click(function(e){
            if(!confirm('Are you sure?')){
                e.preventDefault();
                return false;
            }
            return true;
        });
    });
</script>

@php
    $activateLabel = ($user->is_active) ? 'Deactivate Account' : 'Activate Account';
@endphp

<x-utilitymenu :items="[
        (object) ['text' => 'Update User', 'url' => route('app.edituser', $user->id), 'icon' => 'pencil-alt'] , 
        (object) ['text' => 'Add Note', 'url' => '/app/add-user-note/'.$user->id, 'icon' => 'chat-alt'], 
        (object) ['text' => $activateLabel, 'url' => '/app/toggle-user-activation/'.$user->id, 'icon' => 'minus-circle'], 
    ]" />

<div id="AddUserNoteForm" style="display: none;" class="mb-8 user-note-form">
    <x-subheading text="Add Note to User" />

    {{ Form::open(['url' => '/app/add-note']) }}
    
    <input type="hidden" name="object_type" value="User" hidden>
    <input type="hidden" name="object_id" value="{{ $user->id }}" hidden>
    <input type="hidden" name="redirect" value="back" hidden>

    <div class="flex flex-row">
        <div class="m-2 mb-0 w-11/12">
            <x-forminput :params="[
                'type' => 'textarea',
                'name' => 'note',
                'label' => 'Note'
            ]" /> 
        </div>
        <div class="w-1/12 pt-4 text-xl form-closer text-gray-400">
            <span><x-icon type="x-circle" fill="1" /></span>
        </div>
    </div>

    <x-submitbutton />
    
    {{ Form::close() }}
    
    <div class="m-6"></div>
</div>

<x-subheading text="Details" />
<div class="flex flex-col-reverse md:flex-row lg:flex-row gap-4 w-full">
    
    <div class="md:w-8/12 lg:w-6/12 -mt-5 md:mt-auto z-10 flex-1 ">
        <div class="border-t border-gray-200">
            @php
                $fields = [
                    'Name' => $user->name, 
                    'Account Type' => $user->account_type,
                    'Email' => $user->email,
                    'Since' => $user->created_at->diffForHumans() 
                ];

                if(!empty($user->external_id)){
                    $fields['HCP'] = $user->external_name;
                    $fields[''] = $user->external_id;
                }
                
            @endphp
            <dl>
                @foreach ($fields as $key => $value)
                    
                <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b bg-gray-50">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ $key }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        {{ $value }}
                    </dd>
                </div>
        
                @endforeach

            </dl>
        </div>
    </div>
    <div class="md:w-2/12 lg:w-2/12 text-center z-20">
        <span class="inline-block p-3 bg-gray-50 rounded-full shadow text-center align-middle">
            <x-icon type="user" />
        </span>
    </div>
</div>

<div class="m-8"></div>

<x-subheading text="History" />

@include('partials.historicalNotes')

@endsection