<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>TeoPros Portal - Complete User Registration</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="http://localhost/css/app.css">

        <!-- Scripts -->
        <script src="http://localhost/js/app.js" defer></script>
        <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
        </script>
    </head>
    <body>
        <div class="font-sans text-gray-900 antialiased">
            <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
    <div>
        <a href="/">
                <img src="/images/app-logo.png" id="AppLogo">            </a>
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        <!-- Session Status -->
        
        <!-- Validation Errors -->
        <h3 class="text-center mb-4">Complete your registration by creating a new password.</h3>
        {{  Form::open(['url' => '/complete-registration/'.$emailToken.'?key='.$key]) }}

            <input type="password" name="password" placeholder="Password" class="w-full border-0 bg-gray-100 rounded-lg" />
            <hr class="my-5">
            <input type="password" name="password_confirmation" placeholder="Confirm Password" class="w-full border-0 bg-gray-100 rounded-lg" />
            <div class="text-center">
                <x-submitbutton text="Complete Registration" />
            </div>
        {{ Form::close() }}

    </div>
</div>
        </div>

        <script>
            $(document).ready(function() {
                $('input[name=submit]').click(function(e) {
                    let val1 = $('input[name=password]').val();
                    let val2 = $('input[name=password_confirmation]').val();
                    if(val1 != val2) {
                        e.preventDefault();
                        alert('Passwords do not match');
                        return false;
                    }
                    return true;
                });
            });
        </script>
    </body>
</html>
