@extends('layouts.app', ['header' => 'New Portal Account'])

@section('content')

    @if(!$isEdit)

        {{ Form::open(['url' => '/app/store-account']) }}
    
    @else 

        {{ Form::model($user, ['url' => '/app/update-account/'.$user->id]) }}

    @endif 

    <h3 class="text-yellow-600">Account Info</h3>
    <div class="w-full lg:w-2/6 inline-block">
        <x-forminput :params="['type' => 'select', 'name' => 'account_type', 'value' => $user->account_type ?? '', 'label' => 'Type', 'options' => ['' => '' , 'Customer' => 'Customer']]" />
        
    </div>
    <div class="clearfix"></div>

    <div class="w-full lg:w-2/6 inline-block">
        <x-forminput :params="['type' => 'text', 'name' => 'name', 'label' => 'Name', 'value' => $user->name ?? '']" />
        
    </div>
    <div class="w-full lg:w-2/6 inline-block">
        <x-forminput :params="['type' => 'text', 'name' => 'email', 'label' => 'Email', 'value' => $user->email ?? '']" />
    </div>

    @if(!$isEdit)

    <div class="w-full lg:w-2/6 inline-block">
        <!-- Random Password: <strong>{{ $randomPassword }}</strong> -->
        <x-forminput :params="['type' => 'password', 'name' => 'pass', 'label' => 'Password', 'disabled' => 'disabled', 'value' => $randomPassword]" />
        <span class="text-xs text-gray-600">Random Password: <strong>{{ $randomPassword }}</strong></span>
        <input type="hidden" name="pass" value="{{ $randomPassword }}">
    </div>

    @endif 

    @if(!empty($user))

    <div class="my-4">
        <label>
            <input type="checkbox" name="change_account" value="1" id="AccountChangeIndicator" />
            <span class="ml-2">Update HCP Account</span>
        </label>
    </div>
    @endif
    
    <hr class="my-5">
    
    <div id="HcpAccountLinkContainer">
        <div class="w-full lg:w-4/6 inline-block relative border-b border-gray-300">
            <h3 class="text-yellow-600">Locate in HCP</h3>
            <span class="absolute top-8 left-0">
                <button x-on="SearchHcp" id="HcpSearchSubmit"><x-icon type="search" fill="1" /></button>
            </span>
            <input type="text" id="HcpSearch" class="ml-8 w-full border-0 focus:border-0" placeholder="HCP Search">
        </div>

        <div class="w-full" id="HcpSearchResultsContainer">
            ...
        </div>
    </div>

    <input type="hidden" name="external_name" id="ExternalName" value="{{ $user->external_name ?? '' }}">
    <x-submitbutton />
    {{ Form::close() }} 

    <script>
        
        $(document).ready(function() {

            $('#AccountChangeIndicator').click(function(){
                if(this.checked){
                    $('#HcpAccountLinkContainer').show();
                } else {
                    $('#HcpAccountLinkContainer').hide();
                }
            });

            @if(!empty($user))

            $('#HcpAccountLinkContainer').hide();

            @endif

            //After email is entered, copy it to the search box and search
            $('input[name=email]').change(function (e) { 
                let currentSearchVal = $.trim($('#HcpSearch').val());
                if(currentSearchVal == ''){
                    $('#HcpSearch').val($(this).val());
                    $('#HcpSearchSubmit').click();
                }
            });
            $('input[name=password]')
                .attr('disabled', 'disabled')
                .css('opacity', '0.4');
            $('#HcpSearchSubmit').click(function(e) {
                e.preventDefault();
                $('#HcpSearchResultsContainer')
                    .html('Searching...')
                    .load('/app/search-hcp?q=' + $('#HcpSearch').val()); 
                return false;
            });

            $('#HcpSearchResultsContainer').delegate('input[name=external_id]', 'click', function(e) {
                let external_id = $(this).val();
                let sourceInputId = '#ExternalName_' + external_id;
                $('#ExternalName').val($(sourceInputId).val());
                return true;
            });
        });
    </script>

@endsection