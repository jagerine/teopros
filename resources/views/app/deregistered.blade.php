@extends('layouts.app', ['header' => 'Account Inactive'])

@section('content')
    <h3>
        Your account is currently inactive. Please contact the administrator for more information.
    </h3>
    
@endsection