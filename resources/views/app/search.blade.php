@extends('layouts.app', ['header' => 'Search Results'])

@section('content')

    <h3 class="text-xl mb-5">Search Results for: &quot;{{ $searchTerm }}&quot;</h3>

    <div class="flex flex-row gap-4">
        <div class="w-full md:3/6">
            <x-subheading text="Request Tickets" />

            @foreach ($requestTickets as $requestTicket)
            
                <div class="ticket-search-result m-2 bg-gray-50 py-1 px-3">
                    <span class="text-gray-300"><x-icon type="ticket" /></span>
                    <a href="/tickets/view/{{ $requestTicket->id }}" class="page-link text-lg">Ticket# {{ $requestTicket->id }}</a><br>
                    <small>{{ nl2br(substr($requestTicket->description, 0, 75)) }}...</small>
                </div>

            @endforeach

        </div>
        <div class="w-full md:3/6">
            <x-subheading text="Users" />

            @foreach ($users as $user)
            
                <div class="user-search-result m-2 bg-gray-50 p-1 px-3">
                    <span class="text-gray-300"><x-icon type="user" /></span>
                    <a class="page-link" href="/app/user/{{ $user->id }}">{{ $user->name }}</a><br>
                    <small>{{ $user->email }}</small>
                </div>

            @endforeach

        </div>

    </div>  


@endsection