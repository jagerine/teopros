@extends('layouts.app', ['header' => 'HCP Search'])

@php 

    #
    # Local formatting functions 
    #

    function _customerName($customer)
    {
        if(empty($customer->first_name) and empty($customer->last_name))
        {
            return '(None)';
        }
        return $customer->first_name.' '.$customer->last_name;
    }

    function _customerCompany($customer)
    {
        if(empty($customer->company) )
        {
            return '(None)';
        }
        return $customer->company;
    }

    function _customerPhone($customer)
    {
        if(empty($customer->mobile_number) )
        {
            return '(None)';    
        }
        return '<a class="page-link" href="tel:'.$customer->mobile_number.'">'.$customer->mobile_number.'</a>';
    }

    function _customerLocation($customer)
    {
        if(empty($customer->addresses) )
        {
            return '';
        }
        return $customer->addresses[0]->city.', '.$customer->addresses[0]->state;
    }

    function _customerEmail($customer)
    {
        if(empty($customer->email))
        {
            return '(None)';
        }
        return '<a class="page-link" href="mailto:'.$customer->email.'">'.$customer->email.'</a>';
    }

@endphp

@section('content')

    <div class="m-2">
        <h4>Search Results for &quot;{{ $query }}&quot; </h4>
        
        @php
            $count = 0; 
        @endphp

        @foreach ($customers->customers as $customer)
            
            @php 
                $rowClass = ($count % 2 == 1) ? 'bg-white' : 'bg-gray-50';
                $count++;
            @endphp
            
            <label class="block flex text-xs gap-2 mb-2 {{ $rowClass }}">
                    <div class="flex-none w-1/12 truncate bg-blue-50 p-2 rounded-md text-center">
                        {{ Form::radio('external_id', $customer->id, false) }}
                        <input type="hidden" id="ExternalName_{{ $customer->id }}" value="{{ $customer->company }}" />
                    </div>
                    <div class="flex-1 w-3/12 text-ellipsis truncate p-2">
                        {!! _customerName($customer) !!}
                    </div>
                    <div class="flex-1 w-2/12 text-ellipsis truncate p-2">
                        {!! _customerCompany($customer) !!}
                    </div>
                    <div class="flex-1 w-2/12 text-ellipsis truncate p-2">
                        {!! _customerLocation($customer) !!}
                    </div>
                    <div class="flex-1 w-2/12 text-ellipsis truncate p-2">
                        {!! _customerEmail($customer) !!}
                    </div>
                    <div class="flex-1 w-2/12 text-ellipsis truncate p-2">
                        {!! _customerPhone($customer) !!}
                    </div>
            </label>
        @endforeach
    </div>

@endsection