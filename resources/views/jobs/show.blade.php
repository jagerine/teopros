@extends('layouts.app', ['header' => $job->project_name])

@section('content')
    <x-utilitymenu :items="[
        (object) ['text' => 'Update Work Order', 'url' => '/jobs/edit/'.$job->id, 'icon' => 'pencil-alt'] , 
        //(object) ['text' => 'Delete', 'url' => '/jobs/destroy/'.$job->id, 'icon' => 'trash'] 
    ]" />


<div class="border-t border-gray-200">
    @php
        $fields = [
            'Type' => $job->work_type,
            'Status' => $job->status, 
            'Name' => $job->project_name, 
            'Customer' => '<a class="underline hover:font-bold" href="/customers/show/'.$job->customer_id.'">'.$job->getCustomer()->display_name.'</a>', 
            'Location' => '<span class="address-container mr-2">'.$job->getLocationText().'</span>', 
        ];
        
    @endphp
    <dl>
        @foreach ($fields as $key => $value)
            
        <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b bg-gray-50">
            <dt class="text-sm font-medium text-gray-500">
                {{ $key }}
            </dt>
            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {!! $value !!}
            </dd>
        </div>

        @endforeach
    </dl>
</div>


    <div class="grid grid-cols-1 gap-3 lg:grid-cols-2">
        


    </div>

    <hr>
    <h3 class="text-lg text-yellow-700">File Attachments</h3>
    <div class="grid grid-cols-1 gap-3 lg:grid-cols-2">
        @foreach ($job->getFiles() as $file)
            <div class="flex items-center">
                <div class="flex-shrink-0">
                    <a href="/files/download/{{ $file->id }}">
                        <img class="h-12 w-12 rounded-full" src="{{ $file->getIcon() }}" alt="{{ $file->name }}">
                    </a>
                </div>
                <div class="ml-4">
                    <a href="/files/download/{{ $file->id }}" class="text-sm leading-5 font-medium text-gray-900 hover:underline">
                        {{ $file->name }}
                    </a>
                    <div class="text-sm leading-5 text-gray-500">
                        {{ $file->getSize() }}
                    </div>
                </div>
            </div>
        @endforeach

    
@endsection