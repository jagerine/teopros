@extends('layouts.app', ['header' => 'Work Orders'])

@section('content')
    <x-utilitymenu :items="[
        (object) ['text' => 'Add Work Order', 'url' => route('workorders.create'), 'icon' => 'plus-circle'] 
    ]" />
    
    <div class="flex">
        <div class="flex-auto w-3/12">
            <strong>Filters</strong>
            
        </div>
        <div class="flex-auto w-9/12">
            <x-datatable :data="$workorders" textFilter="1" />
        </div>
    </div>

@endsection