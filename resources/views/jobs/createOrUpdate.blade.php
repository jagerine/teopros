@extends('layouts.app', ['header' => $heading])

@section('content')
    
    @if(isset($job))
        {{ Form::model($job, ['url' => ['/jobs/update/'. $job->id]]) }}
    @else
        {{ Form::open(['url' => '/jobs/store']) }}

    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

        <script>
            $(document).ready(function() {
                $("input[name='location[street]']").val("{{ $customer->getAddress()['street'] }}");
                $("input[name='location[unit]']").val("{{ $customer->getAddress()['unit'] }}");
                $("input[name='location[city]']").val("{{ $customer->getAddress()['city'] }}");
                $("input[name='location[state]']").val("{{ $customer->getAddress()['state'] }}");
                $("input[name='location[zip]']").val("{{ $customer->getAddress()['zip'] }}");
                $('select[name=status]').focus();
            });
        </script>
    @endif

    <h4>New Job for {{ $customer->display_name }}</h4>
    <hr>
    <div class="w-full lg:w-2/6 inline-block">
        <x-forminput :params="['type' => 'select', 'name' => 'status', 'label' => 'Status', 'options' => ['Proposal' => 'Request for Proposal', 'Estimate' => 'Estimate', 'Work Order' => 'Work Order']]" />
        
    </div>
    <div class="w-full lg:w-2/6 inline-block">
        <x-forminput :params="['type' => 'select', 'name' => 'work_type', 'label' => 'Project Type', 'options' => ['Improvement' => 'Improvement', 'Repair' => 'Repair']]" />
        
    </div>
    <div class="w-full lg:w-3/6 inline-block">
        
            <label for="toggleB" class="flex items-center cursor-pointer">
              <!-- toggle -->
              <div class="relative">
                <!-- input -->
                <input type="checkbox" id="toggleB" class="sr-only" name="high_priority">
                <!-- line -->
                <div class="block bg-gray-600 w-14 h-8 rounded-full dotcontainer"></div>
                <!-- dot -->
                <div class="dot absolute left-1 top-1 bg-white w-6 h-6 rounded-full transition"></div>
              </div>
              <!-- label -->
              <div class="ml-3 text-gray-700 font-medium">
                High Priority
              </div>
            </label>

    </div>
    <hr class="my-4"> 
    <div class="w-full lg:w-3/6 inline-block">
        <x-forminput :params="['name' => 'project_name', 'label' => 'Project Name']" /> 
    </div>
    <div class="w-full lg:w-4/6 inline-block">
        <x-forminput :params="['type' => 'longtext', 'name' => 'description', 'label' => 'Project Description']" /> 
    </div>
    <hr class="my-4"> 

        <div class="grid grid-cols-1 md:grid-cols-4">
            <div><x-forminput :params="['type' => 'text', 'name' => 'contact_name', 'label' => 'Contact Name']" /> </div>
            <div><x-forminput :params="['type' => 'email', 'name' => 'contact_email', 'label' => 'Contact Email']" /> </div>
            <div><x-forminput :params="['type' => 'phone', 'name' => 'contact_phone', 'label' => 'Contact Phone']" /> </div>
        </div>

    <hr class="my-4">

    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-72">
            <x-forminput :params="['name' => 'location[street]', 'label' => 'Address']" /> 
        </div>
        <div class="w-full md:w-32">
            <x-forminput :params="['name' => 'location[unit]', 'label' => 'Unit']" /> 
        </div>
    </div>
    
    <div class="flex flex-col md:flex-row">
        <div class="">
            <x-forminput :params="['name' => 'location[city]', 'label' => 'City']" /> 
        </div>
        <div class="">
            <x-forminput :params="['name' => 'location[state]', 'label' => 'ST']" /> 
        </div>
        <div class="">
            <x-forminput :params="['name' => 'location[zip]', 'label' => 'Zip']" /> 
        </div>
    </div>
    <div class="flex flex-col md:flex-row">
        <x-forminput :params="['type' => 'textarea', 'name' => 'location[description]', 'label' => 'Optional Location Description']" /> 
    </div>
    
    <hr class="mt-4 mb-4">

    <h4>Details</h4> 
    <div id="LineItemTemplate" class="invisible hidden display-none">
        <div class="flex flex-col md:flex-row line-item-row">
            <div class="w-full md:w-72">
                <x-forminput :params="['name' => 'line_items[0][description]', 'label' => 'Description']" /> 
            </div>
            <div class="w-full md:w-32">
                <x-forminput :params="['name' => 'line_items[0][quantity]', 'label' => 'Quantity']" /> 
            </div>
            <div class="w-full md:w-32">
                <x-forminput :params="['name' => 'line_items[0][price]', 'label' => 'Price']" /> 
            </div>
            <div class="w-full pt-0 pb-8 md:w-32 md:pt-8 md:pb-0 text-gray-500 cursor-pointer line-item-remove">
                <x-icon type="x-circle" fill="1" />
            </div>
        </div>
    </div>
    <div id="WriteRoot"></div>

    
    <button id="AddNew" class="border border-gray-300 bg-blue-50 rounded-md cursor-pointer hover:bg-blue-100 py-1 px-3 mt-3">
        <x-icon type="plus-circle" fill="1" />
        Add
    </button>  

        <hr class="mt-4 mb-4">

        <div class="grid grid-cols-1 md:grid-cols-4">
            <div><x-forminput :params="['name' => 'monetary_limit', 'placeholder' => 'Monetary Limit']" /> </div>
            <div><x-forminput :params="['type' => 'date', 'name' => 'complete_by', 'placeholder' => 'Complete By']" /> </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-4">
            <div><x-forminput :params="['type' => 'date', 'name' => 'service_start', 'placeholder' => 'Service Start']" /> </div>
            <div><x-forminput :params="['type' => 'date', 'name' => 'service_end', 'placeholder' => 'Service End']" /> </div>
        </div>

        <x-submitbutton />

        {{ Form::close() }}

    <script>
        var LineItemCount = 1;
        function _addLineItem(){
            var lineItem = document.createElement('div');
            var tplHtml = $('#LineItemTemplate').html();
            lineItem.className = 'flex flex-col md:flex-row';
            lineItem.innerHTML = tplHtml;

            $(lineItem).find('input[data-label="Description"]').attr('name', 'line_items['+LineItemCount+'][description]');
            $(lineItem).find('input[data-label="Quantity"]').attr('name', 'line_items['+LineItemCount+'][quantity]');
            $(lineItem).find('input[data-label="Price"]').attr('name', 'line_items['+LineItemCount+'][price]');

            LineItemCount++;
            document.getElementById('WriteRoot').appendChild(lineItem);
        }

        $(document).ready(function(){
            $('#AddNew').click(function(){
                _addLineItem();
                return false;
            });
            $('body').delegate('.line-item-remove > svg', 'click', function(e){
                $(e.currentTarget).parents('.line-item-row').remove();
                return false;
            });
            _addLineItem();
            $('#WriteRoot').find('.line-item-remove').remove();
            
        });
    </script>
    <style>
        input:checked ~ .dotcontainer{
            background-color: #ddd;
            transition-duration: 200ms;
        }
        input:checked ~ .dot {
            transform: translateX(100%);
            background-color: #487ebb;
        }
    </style>

@endsection